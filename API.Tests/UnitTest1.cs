using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using RouletteBackend.BusinessLogic;
using RouletteBackend.BusinessLogic.ApiResponses;

namespace API.Tests;

public class Tests
{
    private RandomHandler? random;
    
    [SetUp]
    public void Setup()
    {
        random = new RandomHandler();
    }

    [Test]
    public void Test1()
    {
        var disguises = new List<Disguise>
        {
            new() { Name = "Architect", Chosen = false },
            new() { Name = "Ark Member", Chosen = false },
            new() { Name = "Blake Nathaniel", Chosen = false },
            new() { Name = "Burial Robes", Chosen = false },
            new() { Name = "Butler", Chosen = false },
            new() { Name = "Castle Staff", Chosen = false },
            new() { Name = "Chef", Chosen = false },
            new() { Name = "Custodian", Chosen = false },
            new() { Name = "Elite Guard", Chosen = false },
            new() { Name = "Entertainer", Chosen = false },
            new() { Name = "Event Staff", Chosen = false },
            new() { Name = "Guard", Chosen = false },
            new() { Name = "Initiate", Chosen = false },
            new() { Name = "Jebediah Block", Chosen = false },
            new() { Name = "Knight's Armor", Chosen = false },
            new() { Name = "Master of Ceremonies", Chosen = false },
            new() { Name = "Radier", Chosen = false },
            new() { Name = "Tuxedo and Mask", Chosen = false },
        };

        var results = new Dictionary<string, int>();
        disguises.ForEach(x => results[x.Name] = 0);
        for (var i = 0; i < 10000; i++)
        {
            results[random!.GetRandomElementFromList(disguises.Where(x => !x.Chosen).ToList()).Name]++;
        }
        
        foreach (var (key, value) in results)
        {
            Console.WriteLine($"{key}: {value}");
        }
    }
}