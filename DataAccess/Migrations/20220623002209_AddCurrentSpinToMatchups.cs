﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddCurrentSpinToMatchups : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CurrentSpinId",
                table: "Matchup",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Matchup_CurrentSpinId",
                table: "Matchup",
                column: "CurrentSpinId");

            migrationBuilder.AddForeignKey(
                name: "FK_Matchup_SavedSpin_CurrentSpinId",
                table: "Matchup",
                column: "CurrentSpinId",
                principalTable: "SavedSpin",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Matchup_SavedSpin_CurrentSpinId",
                table: "Matchup");

            migrationBuilder.DropIndex(
                name: "IX_Matchup_CurrentSpinId",
                table: "Matchup");

            migrationBuilder.DropColumn(
                name: "CurrentSpinId",
                table: "Matchup");
        }
    }
}
