﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddSavedSpinTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SavedDisguise",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TileUrl = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SavedDisguise", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SavedKillMethod",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TileUrl = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SelectedVariant = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SavedKillMethod", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SavedSpin",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PublicId = table.Column<int>(type: "int", nullable: false),
                    MissionId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SavedSpin", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SavedSpin_Mission_MissionId",
                        column: x => x.MissionId,
                        principalTable: "Mission",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SavedTargetCondition",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SpinId = table.Column<int>(type: "int", nullable: false),
                    TargetId = table.Column<int>(type: "int", nullable: false),
                    KillMethodId = table.Column<int>(type: "int", nullable: false),
                    DisguiseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SavedTargetCondition", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SavedTargetCondition_SavedDisguise_DisguiseId",
                        column: x => x.DisguiseId,
                        principalTable: "SavedDisguise",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_SavedTargetCondition_SavedKillMethod_KillMethodId",
                        column: x => x.KillMethodId,
                        principalTable: "SavedKillMethod",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SavedTargetCondition_SavedSpin_SpinId",
                        column: x => x.SpinId,
                        principalTable: "SavedSpin",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SavedTargetCondition_Target_TargetId",
                        column: x => x.TargetId,
                        principalTable: "Target",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SavedSpin_MissionId",
                table: "SavedSpin",
                column: "MissionId");

            migrationBuilder.CreateIndex(
                name: "IX_SavedTargetCondition_DisguiseId",
                table: "SavedTargetCondition",
                column: "DisguiseId");

            migrationBuilder.CreateIndex(
                name: "IX_SavedTargetCondition_KillMethodId",
                table: "SavedTargetCondition",
                column: "KillMethodId");

            migrationBuilder.CreateIndex(
                name: "IX_SavedTargetCondition_SpinId",
                table: "SavedTargetCondition",
                column: "SpinId");

            migrationBuilder.CreateIndex(
                name: "IX_SavedTargetCondition_TargetId",
                table: "SavedTargetCondition",
                column: "TargetId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SavedTargetCondition");

            migrationBuilder.DropTable(
                name: "SavedDisguise");

            migrationBuilder.DropTable(
                name: "SavedKillMethod");

            migrationBuilder.DropTable(
                name: "SavedSpin");
        }
    }
}
