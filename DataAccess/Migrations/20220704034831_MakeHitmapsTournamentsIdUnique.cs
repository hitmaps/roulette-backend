﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class MakeHitmapsTournamentsIdUnique : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Matchup_HitmapsTournamentsId",
                table: "Matchup",
                column: "HitmapsTournamentsId",
                unique: true,
                filter: "[HitmapsTournamentsId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Matchup_HitmapsTournamentsId",
                table: "Matchup");
        }
    }
}
