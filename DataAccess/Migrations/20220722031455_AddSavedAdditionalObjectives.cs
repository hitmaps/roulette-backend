﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddSavedAdditionalObjectives : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SavedAdditionalObjectiveOption",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TileUrl = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SavedAdditionalObjectiveOption", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SavedAdditionalObjective",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SpinId = table.Column<int>(type: "int", nullable: false),
                    AdditionalObjectiveId = table.Column<int>(type: "int", nullable: false),
                    CompletionMethodId = table.Column<int>(type: "int", nullable: false),
                    DisguiseId = table.Column<int>(type: "int", nullable: true),
                    SavedSpinId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SavedAdditionalObjective", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SavedAdditionalObjective_AdditionalObjective_AdditionalObjectiveId",
                        column: x => x.AdditionalObjectiveId,
                        principalTable: "AdditionalObjective",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SavedAdditionalObjective_SavedAdditionalObjectiveOption_CompletionMethodId",
                        column: x => x.CompletionMethodId,
                        principalTable: "SavedAdditionalObjectiveOption",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SavedAdditionalObjective_SavedDisguise_DisguiseId",
                        column: x => x.DisguiseId,
                        principalTable: "SavedDisguise",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_SavedAdditionalObjective_SavedSpin_SavedSpinId",
                        column: x => x.SavedSpinId,
                        principalTable: "SavedSpin",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_SavedAdditionalObjective_SavedSpin_SpinId",
                        column: x => x.SpinId,
                        principalTable: "SavedSpin",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SavedAdditionalObjective_AdditionalObjectiveId",
                table: "SavedAdditionalObjective",
                column: "AdditionalObjectiveId");

            migrationBuilder.CreateIndex(
                name: "IX_SavedAdditionalObjective_CompletionMethodId",
                table: "SavedAdditionalObjective",
                column: "CompletionMethodId");

            migrationBuilder.CreateIndex(
                name: "IX_SavedAdditionalObjective_DisguiseId",
                table: "SavedAdditionalObjective",
                column: "DisguiseId");

            migrationBuilder.CreateIndex(
                name: "IX_SavedAdditionalObjective_SavedSpinId",
                table: "SavedAdditionalObjective",
                column: "SavedSpinId");

            migrationBuilder.CreateIndex(
                name: "IX_SavedAdditionalObjective_SpinId",
                table: "SavedAdditionalObjective",
                column: "SpinId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SavedAdditionalObjective");

            migrationBuilder.DropTable(
                name: "SavedAdditionalObjectiveOption");
        }
    }
}
