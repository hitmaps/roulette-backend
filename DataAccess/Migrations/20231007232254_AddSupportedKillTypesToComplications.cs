﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddSupportedKillTypesToComplications : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ComplicationToKillType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ComplicationId = table.Column<int>(type: "int", nullable: false),
                    KillType = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComplicationToKillType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ComplicationToKillType_Complication_ComplicationId",
                        column: x => x.ComplicationId,
                        principalTable: "Complication",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ComplicationToKillType_ComplicationId",
                table: "ComplicationToKillType",
                column: "ComplicationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ComplicationToKillType");
        }
    }
}
