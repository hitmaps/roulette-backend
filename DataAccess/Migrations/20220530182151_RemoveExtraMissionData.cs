﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class RemoveExtraMissionData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LocationBackgroundUrl",
                table: "Mission");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Mission");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LocationBackgroundUrl",
                table: "Mission",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Mission",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
