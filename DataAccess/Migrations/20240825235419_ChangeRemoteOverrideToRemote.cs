﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class ChangeRemoteOverrideToRemote : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RemoteOverride",
                table: "KillVariant");

            migrationBuilder.AddColumn<bool>(
                name: "Remote",
                table: "KillVariant",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Remote",
                table: "KillVariant");

            migrationBuilder.AddColumn<bool>(
                name: "RemoteOverride",
                table: "KillVariant",
                type: "bit",
                nullable: true);
        }
    }
}
