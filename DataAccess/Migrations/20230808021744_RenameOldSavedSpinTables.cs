﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class RenameOldSavedSpinTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "SavedAdditionalObjective",
                newName: "OldSavedAdditionalObjective");
            
            migrationBuilder.RenameTable(
                name: "SavedTargetCondition",
                newName: "OldSavedTargetCondition");
            
            migrationBuilder.RenameTable(
                name: "SavedAdditionalObjectiveOption",
                newName: "OldSavedAdditionalObjectiveOption");
            
            migrationBuilder.RenameTable(
                name: "SavedDisguise",
                newName: "OldSavedDisguise");
            
            migrationBuilder.RenameTable(
                name: "SavedKillMethod",
                newName: "OldSavedKillMethod");
            
            migrationBuilder.RenameTable(
                name: "SavedSpin",
                newName: "OldSavedSpin");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "OldSavedAdditionalObjective",
                newName: "SavedAdditionalObjective");
            
            migrationBuilder.RenameTable(
                name: "OldSavedTargetCondition",
                newName: "SavedTargetCondition");
            
            migrationBuilder.RenameTable(
                name: "OldSavedAdditionalObjectiveOption",
                newName: "SavedAdditionalObjectiveOption");
            
            migrationBuilder.RenameTable(
                name: "OldSavedDisguise",
                newName: "SavedDisguise");
            
            migrationBuilder.RenameTable(
                name: "OldSavedKillMethod",
                newName: "SavedKillMethod");
            
            migrationBuilder.RenameTable(
                name: "OldSavedSpin",
                newName: "SavedSpin");
        }
    }
}
