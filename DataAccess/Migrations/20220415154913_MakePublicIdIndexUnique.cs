﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class MakePublicIdIndexUnique : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SavedSpin_PublicId",
                table: "SavedSpin");

            migrationBuilder.CreateIndex(
                name: "IX_SavedSpin_PublicId",
                table: "SavedSpin",
                column: "PublicId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SavedSpin_PublicId",
                table: "SavedSpin");

            migrationBuilder.CreateIndex(
                name: "IX_SavedSpin_PublicId",
                table: "SavedSpin",
                column: "PublicId");
        }
    }
}
