﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddMapSelectionsToMatchup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MatchupMission");

            migrationBuilder.CreateTable(
                name: "MapSelection",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MatchupId = table.Column<int>(type: "int", nullable: false),
                    MissionId = table.Column<int>(type: "int", nullable: false),
                    ChosenByName = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MapSelection", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MapSelection_Matchup_MatchupId",
                        column: x => x.MatchupId,
                        principalTable: "Matchup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MapSelection_Mission_MissionId",
                        column: x => x.MissionId,
                        principalTable: "Mission",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MapSelection_MatchupId",
                table: "MapSelection",
                column: "MatchupId");

            migrationBuilder.CreateIndex(
                name: "IX_MapSelection_MissionId",
                table: "MapSelection",
                column: "MissionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MapSelection");

            migrationBuilder.CreateTable(
                name: "MatchupMission",
                columns: table => new
                {
                    MatchupsId = table.Column<int>(type: "int", nullable: false),
                    MissionsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchupMission", x => new { x.MatchupsId, x.MissionsId });
                    table.ForeignKey(
                        name: "FK_MatchupMission_Matchup_MatchupsId",
                        column: x => x.MatchupsId,
                        principalTable: "Matchup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MatchupMission_Mission_MissionsId",
                        column: x => x.MissionsId,
                        principalTable: "Mission",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MatchupMission_MissionsId",
                table: "MatchupMission",
                column: "MissionsId");
        }
    }
}
