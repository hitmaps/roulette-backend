﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddMatchupTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SavedTargetCondition_Target_TargetId",
                table: "SavedTargetCondition");

            migrationBuilder.CreateTable(
                name: "Matchup",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MatchupId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    HitmapsTournamentsId = table.Column<int>(type: "int", nullable: true),
                    TimerFingerprint = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TimerPaused = table.Column<bool>(type: "bit", nullable: false),
                    TimerPausedRemainingTimeInSeconds = table.Column<int>(type: "int", nullable: true),
                    TimerStartTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TimerEndTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Matchup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Player",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MatchupId = table.Column<int>(type: "int", nullable: false),
                    PublicId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LastPing = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CompleteTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Player", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Player_Matchup_MatchupId",
                        column: x => x.MatchupId,
                        principalTable: "Matchup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Matchup_MatchupId",
                table: "Matchup",
                column: "MatchupId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Player_MatchupId",
                table: "Player",
                column: "MatchupId");

            migrationBuilder.AddForeignKey(
                name: "FK_SavedTargetCondition_Target_TargetId",
                table: "SavedTargetCondition",
                column: "TargetId",
                principalTable: "Target",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SavedTargetCondition_Target_TargetId",
                table: "SavedTargetCondition");

            migrationBuilder.DropTable(
                name: "Player");

            migrationBuilder.DropTable(
                name: "Matchup");

            migrationBuilder.AddForeignKey(
                name: "FK_SavedTargetCondition_Target_TargetId",
                table: "SavedTargetCondition",
                column: "TargetId",
                principalTable: "Target",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
