﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AttachSavedSpinsToMapSelections : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SavedSpinId",
                table: "MapSelection",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MapSelection_SavedSpinId",
                table: "MapSelection",
                column: "SavedSpinId");

            migrationBuilder.AddForeignKey(
                name: "FK_MapSelection_SavedSpin_SavedSpinId",
                table: "MapSelection",
                column: "SavedSpinId",
                principalTable: "SavedSpin",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MapSelection_SavedSpin_SavedSpinId",
                table: "MapSelection");

            migrationBuilder.DropIndex(
                name: "IX_MapSelection_SavedSpinId",
                table: "MapSelection");

            migrationBuilder.DropColumn(
                name: "SavedSpinId",
                table: "MapSelection");
        }
    }
}
