﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddAdditionalObjectives : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AdditionalObjective",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MissionId = table.Column<int>(type: "int", nullable: false),
                    TileUrl = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdditionalObjective", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdditionalObjective_Mission_MissionId",
                        column: x => x.MissionId,
                        principalTable: "Mission",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AdditionalObjectiveOption",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AdditionalObjectiveId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TileUrl = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdditionalObjectiveOption", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdditionalObjectiveOption_AdditionalObjective_AdditionalObjectiveId",
                        column: x => x.AdditionalObjectiveId,
                        principalTable: "AdditionalObjective",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdditionalObjective_MissionId",
                table: "AdditionalObjective",
                column: "MissionId");

            migrationBuilder.CreateIndex(
                name: "IX_AdditionalObjectiveOption_AdditionalObjectiveId",
                table: "AdditionalObjectiveOption",
                column: "AdditionalObjectiveId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdditionalObjectiveOption");

            migrationBuilder.DropTable(
                name: "AdditionalObjective");
        }
    }
}
