﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddAllowsWeaponVariantsToTargetAndRefactorKillTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_KillVariant_SpecificFirearmKill_SpecificFirearmKillId",
                table: "KillVariant");

            migrationBuilder.DropTable(
                name: "AccidentKill");

            migrationBuilder.DropTable(
                name: "GenericKill");

            migrationBuilder.DropTable(
                name: "MeleeKill");

            migrationBuilder.DropTable(
                name: "SpecificFirearmKill");

            migrationBuilder.DropTable(
                name: "UniqueKill");

            migrationBuilder.DropColumn(
                name: "Chosen",
                table: "KillVariant");

            migrationBuilder.RenameColumn(
                name: "SpecificFirearmKillId",
                table: "KillVariant",
                newName: "KillId");

            migrationBuilder.RenameIndex(
                name: "IX_KillVariant_SpecificFirearmKillId",
                table: "KillVariant",
                newName: "IX_KillVariant_KillId");

            migrationBuilder.AddColumn<bool>(
                name: "AllowsWeaponVariants",
                table: "Target",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Kill",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TileUrl = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LargeWeapon = table.Column<bool>(type: "bit", nullable: false),
                    Remote = table.Column<bool>(type: "bit", nullable: false),
                    Type = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Kill", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_KillVariant_Kill_KillId",
                table: "KillVariant",
                column: "KillId",
                principalTable: "Kill",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_KillVariant_Kill_KillId",
                table: "KillVariant");

            migrationBuilder.DropTable(
                name: "Kill");

            migrationBuilder.DropColumn(
                name: "AllowsWeaponVariants",
                table: "Target");

            migrationBuilder.RenameColumn(
                name: "KillId",
                table: "KillVariant",
                newName: "SpecificFirearmKillId");

            migrationBuilder.RenameIndex(
                name: "IX_KillVariant_KillId",
                table: "KillVariant",
                newName: "IX_KillVariant_SpecificFirearmKillId");

            migrationBuilder.AddColumn<bool>(
                name: "Chosen",
                table: "KillVariant",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "AccidentKill",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TileUrl = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccidentKill", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GenericKill",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LargeWeapon = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TileUrl = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenericKill", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MeleeKill",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MissionId = table.Column<int>(type: "int", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TileUrl = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeleeKill", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MeleeKill_Mission_MissionId",
                        column: x => x.MissionId,
                        principalTable: "Mission",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "SpecificFirearmKill",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Chosen = table.Column<bool>(type: "bit", nullable: false),
                    LargeWeapon = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TileUrl = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpecificFirearmKill", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UniqueKill",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TargetId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TileUrl = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UniqueKill", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UniqueKill_Target_TargetId",
                        column: x => x.TargetId,
                        principalTable: "Target",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MeleeKill_MissionId",
                table: "MeleeKill",
                column: "MissionId");

            migrationBuilder.CreateIndex(
                name: "IX_UniqueKill_TargetId",
                table: "UniqueKill",
                column: "TargetId");

            migrationBuilder.AddForeignKey(
                name: "FK_KillVariant_SpecificFirearmKill_SpecificFirearmKillId",
                table: "KillVariant",
                column: "SpecificFirearmKillId",
                principalTable: "SpecificFirearmKill",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
