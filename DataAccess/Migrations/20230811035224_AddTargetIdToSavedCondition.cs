﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddTargetIdToSavedCondition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("UPDATE dbo.Matchup SET CurrentSpinId = NULL");
            migrationBuilder.DropForeignKey(
                name: "FK_Matchup_OldSavedSpin_CurrentSpinId",
                table: "Matchup");

            migrationBuilder.AddColumn<int>(
                name: "EntityId",
                table: "SavedCondition",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_Matchup_SavedSpin_CurrentSpinId",
                table: "Matchup",
                column: "CurrentSpinId",
                principalTable: "SavedSpin",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Matchup_SavedSpin_CurrentSpinId",
                table: "Matchup");

            migrationBuilder.DropColumn(
                name: "EntityId",
                table: "SavedCondition");

            migrationBuilder.AddForeignKey(
                name: "FK_Matchup_OldSavedSpin_CurrentSpinId",
                table: "Matchup",
                column: "CurrentSpinId",
                principalTable: "OldSavedSpin",
                principalColumn: "Id");
        }
    }
}
