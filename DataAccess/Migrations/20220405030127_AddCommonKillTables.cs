﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddCommonKillTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccidentKill",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TileUrl = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccidentKill", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GenericKill",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LargeWeapon = table.Column<bool>(type: "bit", nullable: false),
                    TileUrl = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenericKill", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MeleeKill",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TileUrl = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MissionId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeleeKill", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MeleeKill_Mission_MissionId",
                        column: x => x.MissionId,
                        principalTable: "Mission",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "SpecificFirearmKill",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LargeWeapon = table.Column<bool>(type: "bit", nullable: false),
                    TileUrl = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Chosen = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpecificFirearmKill", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "KillVariant",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SpecificFirearmKillId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LargeWeapon = table.Column<bool>(type: "bit", nullable: false),
                    Chosen = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KillVariant", x => x.Id);
                    table.ForeignKey(
                        name: "FK_KillVariant_SpecificFirearmKill_SpecificFirearmKillId",
                        column: x => x.SpecificFirearmKillId,
                        principalTable: "SpecificFirearmKill",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_KillVariant_SpecificFirearmKillId",
                table: "KillVariant",
                column: "SpecificFirearmKillId");

            migrationBuilder.CreateIndex(
                name: "IX_MeleeKill_MissionId",
                table: "MeleeKill",
                column: "MissionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccidentKill");

            migrationBuilder.DropTable(
                name: "GenericKill");

            migrationBuilder.DropTable(
                name: "KillVariant");

            migrationBuilder.DropTable(
                name: "MeleeKill");

            migrationBuilder.DropTable(
                name: "SpecificFirearmKill");
        }
    }
}
