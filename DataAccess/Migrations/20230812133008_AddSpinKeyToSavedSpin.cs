﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddSpinKeyToSavedSpin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SpinKey",
                table: "SavedSpin",
                type: "nvarchar(256)",
                maxLength: 256,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_SavedSpin_SpinKey",
                table: "SavedSpin",
                column: "SpinKey",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SavedSpin_SpinKey",
                table: "SavedSpin");

            migrationBuilder.DropColumn(
                name: "SpinKey",
                table: "SavedSpin");
        }
    }
}
