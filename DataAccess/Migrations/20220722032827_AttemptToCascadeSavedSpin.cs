﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AttemptToCascadeSavedSpin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SavedAdditionalObjective_SavedSpin_SpinId",
                table: "SavedAdditionalObjective");

            migrationBuilder.AddForeignKey(
                name: "FK_SavedAdditionalObjective_SavedSpin_SpinId",
                table: "SavedAdditionalObjective",
                column: "SpinId",
                principalTable: "SavedSpin",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SavedAdditionalObjective_SavedSpin_SpinId",
                table: "SavedAdditionalObjective");

            migrationBuilder.AddForeignKey(
                name: "FK_SavedAdditionalObjective_SavedSpin_SpinId",
                table: "SavedAdditionalObjective",
                column: "SpinId",
                principalTable: "SavedSpin",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
