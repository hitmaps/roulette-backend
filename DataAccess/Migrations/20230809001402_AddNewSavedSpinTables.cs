﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddNewSavedSpinTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SavedSpin",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PublicId = table.Column<long>(type: "bigint", nullable: false),
                    MissionId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SavedSpin", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SavedSpin_Mission_MissionId",
                        column: x => x.MissionId,
                        principalTable: "Mission",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SavedCondition",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SavedSpinId = table.Column<int>(type: "int", nullable: false),
                    ConditionType = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ConditionValue = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SavedCondition", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SavedCondition_SavedSpin_SavedSpinId",
                        column: x => x.SavedSpinId,
                        principalTable: "SavedSpin",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SavedCondition_SavedSpinId",
                table: "SavedCondition",
                column: "SavedSpinId");

            migrationBuilder.CreateIndex(
                name: "IX_SavedSpin_MissionId",
                table: "SavedSpin",
                column: "MissionId");

            migrationBuilder.CreateIndex(
                name: "IX_SavedSpin_PublicId",
                table: "SavedSpin",
                column: "PublicId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SavedCondition");

            migrationBuilder.DropTable(
                name: "SavedSpin");
        }
    }
}
