﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddMissionsToMatchup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MatchupMission",
                columns: table => new
                {
                    MatchupsId = table.Column<int>(type: "int", nullable: false),
                    MissionsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchupMission", x => new { x.MatchupsId, x.MissionsId });
                    table.ForeignKey(
                        name: "FK_MatchupMission_Matchup_MatchupsId",
                        column: x => x.MatchupsId,
                        principalTable: "Matchup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MatchupMission_Mission_MissionsId",
                        column: x => x.MissionsId,
                        principalTable: "Mission",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MatchupMission_MissionsId",
                table: "MatchupMission",
                column: "MissionsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MatchupMission");
        }
    }
}
