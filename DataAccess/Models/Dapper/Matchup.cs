namespace DataAccess.Models.Dapper;

public class Matchup
{
    public int Id { get; set; }
    public Guid MatchupId { get; set; }
    public int? HitmapsTournamentsId { get; set; }
    // TODO Add other fields as needed
}