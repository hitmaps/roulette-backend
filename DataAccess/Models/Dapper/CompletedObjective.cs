namespace DataAccess.Models.Dapper;

public class CompletedObjective
{
    public int MatchupId { get; set; }
    public string PlayerName { get; set; } = null!;
    public int ObjectiveIndex { get; set; }
    public bool Completed { get; set; }
}