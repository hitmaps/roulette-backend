using System.ComponentModel.DataAnnotations;

namespace DataAccess.Models.EF;

public class Mission
{
    public int Id { get; set; }
    public string Slug { get; set; } = null!;
    [MaxLength(255)]
    public string VariantSlug { get; set; } = null!;
    public int PublicIdPrefix { get; set; }
    public bool SupportsContractsMode { get; set; }
    public List<Target> Targets { get; set; } = new();
    public List<AdditionalObjective> AdditionalObjectives { get; set; } = new();
}