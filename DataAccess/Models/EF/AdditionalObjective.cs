namespace DataAccess.Models.EF;

public class AdditionalObjective
{
    public int Id { get; set; }
    public Mission Mission { get; set; } = null!;
    public string Name { get; set; } = null!;
    public List<AdditionalObjectiveOption> CompletionOptions { get; set; } = new();
    public string TileUrl { get; set; } = null!;
}