namespace DataAccess.Models.EF;

public class OldSavedDisguise
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public string TileUrl { get; set; } = null!;
}