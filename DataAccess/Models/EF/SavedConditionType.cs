namespace DataAccess.Models.EF;

public class SavedConditionType
{
    public string Value { get; }

    private SavedConditionType(string value)
    {
        Value = value;
    }

    public static SavedConditionType TargetKill => new SavedConditionType("TARGET|KILL");
    public static SavedConditionType TargetDisguise => new SavedConditionType("TARGET|DISGUISE");
    public static SavedConditionType TargetComplication => new SavedConditionType("TARGET|COMPLICATION");
    public static SavedConditionType AdditionalObjectiveCompletionMethod => new SavedConditionType("ADDITIONAL-OBJECTIVE|COMPLETION-METHOD");
    public static SavedConditionType AdditionalObjectiveDisguise = new SavedConditionType("ADDITIONAL-OBJECTIVE|DISGUISE");

    private static List<SavedConditionType> allTypes = new()
    {
        TargetKill, TargetDisguise, TargetComplication, 
        AdditionalObjectiveDisguise, AdditionalObjectiveCompletionMethod
    };

    public static SavedConditionType FromValue(string value)
    {
        return allTypes.FirstOrDefault(x => x.Value == value) ??
               throw new InvalidOperationException($"Could not find SavedConditionType with value '{value}'");
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != GetType()) return false;
        return Value == (obj as SavedConditionType)!.Value;
    }

    public override int GetHashCode()
    {
        return Value.GetHashCode();
    }
}