namespace DataAccess.Models.EF;

public class OldSavedKillMethod
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public string TileUrl { get; set; } = null!;
    public string? SelectedVariant { get; set; }
}