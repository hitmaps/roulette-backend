namespace DataAccess.Models.EF;

public class OldSavedAdditionalObjective
{
    public int Id { get; set; }
    public OldSavedSpin Spin { get; set; } = null!;
    public AdditionalObjective AdditionalObjective { get; set; } = null!;
    public OldSavedAdditionalObjectiveOption CompletionMethod { get; set; } = null!;
    public OldSavedDisguise? Disguise { get; set; }
}