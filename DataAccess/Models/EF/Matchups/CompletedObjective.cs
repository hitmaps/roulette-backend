namespace DataAccess.Models.EF.Matchups;

public class CompletedObjective
{
    public int Id { get; set; }
    public Matchup Matchup { get; set; } = null!;
    public string PlayerName { get; set; } = null!;
    public int ObjectiveIndex { get; set; }
}