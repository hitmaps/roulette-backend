namespace DataAccess.Models.EF.Matchups;

public class MapSelection
{
    public int Id { get; set; }
    public Matchup Matchup { get; set; } = null!;
    public Mission Mission { get; set; } = null!;
    public string? ChosenByName { get; set; }
    public bool Complete { get; set; }
    public string? WinnerName { get; set; }
    public DateTime? MapStartedAt { get; set; }
    public DateTime? WinnerFinishedAt { get; set; }
    public DateTime? ResultVerifiedAt { get; set; }
    public SavedSpin? SavedSpin { get; set; }
}