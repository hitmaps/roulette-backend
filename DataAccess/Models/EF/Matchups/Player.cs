namespace DataAccess.Models.EF.Matchups;

public class Player
{
    public int Id { get; set; }
    public Matchup Matchup { get; set; } = null!;
    public string PublicId { get; set; } = null!;
    public string Name { get; set; } = null!;
    public string? AvatarUrl { get; set; }
    public DateTime LastPing { get; set; }
    public DateTime? CompleteTime { get; set; }
    public List<Message> Messages { get; set; } = new();
    public bool Forfeit { get; set; }
}