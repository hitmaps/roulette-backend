namespace DataAccess.Models.EF.Matchups;

public class Message
{
    public int Id { get; set; }
    public Player Player { get; set; } = null!;
    public DateTime SentAt { get; set; }
    public string Content { get; set; } = null!;
    public DateTime? ReceivedAt { get; set; }
    public DateTime? AcknowledgedAt { get; set; }
}