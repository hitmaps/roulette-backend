namespace DataAccess.Models.EF.Matchups;

public class Matchup
{
    public int Id { get; set; }
    public Guid MatchupId { get; set; }
    public int? HitmapsTournamentsId { get; set; }
    public List<Player> Players { get; set; } = new();
    public List<MapSelection> MapSelections { get; set; } = new();
    public Guid TimerFingerprint { get; set; }
    public bool TimerPaused { get; set; }
    public int? TimerPausedRemainingTimeInSeconds { get; set; }
    public DateTime? TimerStartTime { get; set; }
    public DateTime? TimerEndTime { get; set; }
    public SavedSpin? CurrentSpin { get; set; }
    public string? OverlayTheme { get; set; }
    public int? PointsForVictory { get; set; }
    public List<CompletedObjective> CompletedObjectives { get; set; } = new();
}