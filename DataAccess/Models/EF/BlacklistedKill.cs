namespace DataAccess.Models.EF;

public class BlacklistedKill
{
    public int Id { get; set; }
    public Target Target { get; set; } = null!;
    public string? KillName { get; set; }
    public string? Disguise { get; set; }
    public string? Complication { get; set; }
}