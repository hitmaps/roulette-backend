namespace DataAccess.Models.EF;

public class AdditionalObjectiveOption
{
    public int Id { get; set; }
    public AdditionalObjective AdditionalObjective { get; set; } = null!;
    public string Name { get; set; } = null!;
    public string TileUrl { get; set; } = null!;
}