namespace DataAccess.Models.EF;

public class SavedCondition
{
    public int Id { get; set; }
    public SavedSpin SavedSpin { get; set; } = null!;
    public int EntityId { get; set; }
    public string ConditionType { get; set; } = null!;
    public string ConditionValue { get; set; } = null!;
}