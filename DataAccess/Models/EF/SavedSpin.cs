using System.ComponentModel.DataAnnotations;

namespace DataAccess.Models.EF;

public class SavedSpin
{
    public int Id { get; set; }
    public long PublicId { get; set; }
    public Mission Mission { get; set; } = null!;
    public List<SavedCondition> SavedConditions { get; set; } = new();
    [MaxLength(length: 64)]
    public string SpinKey { get; set; } = null!;
}