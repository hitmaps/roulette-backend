namespace DataAccess.Models.EF;

public enum KillType
{
    Generic = 0,
    SpecificFirearm = 1,
    SpecificAccidentOrPoison = 2,
    SpecificMelee = 3
}