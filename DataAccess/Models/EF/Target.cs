namespace DataAccess.Models.EF;

public class Target
{
    public int Id { get; set; }
    public Mission Mission { get; set; } = null!;
    public string Name { get; set; } = null!;
    public string TileUrl { get; set; } = null!;
    public List<BlacklistedKill> BlacklistedKills { get; set; } = new();
    public List<UniqueKill> UniqueKills { get; set; } = new();
    public bool AllowsWeaponVariants { get; set; } = true;
    public List<KillOverride> KillOverrides { get; set; } = new();
}