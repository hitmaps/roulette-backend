namespace DataAccess.Models.EF;

public class KillVariant
{
    public int Id { get; set; }
    public Kill Kill { get; set; } = null!;
    public string Name { get; set; } = null!;
    public string? ImageOverride { get; set; }
    public bool LargeWeapon { get; set; }
    public bool Remote { get; set; }
}