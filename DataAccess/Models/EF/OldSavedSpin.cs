namespace DataAccess.Models.EF;

public class OldSavedSpin
{
    public int Id { get; set; }
    public long PublicId { get; set; }
    public Mission Mission { get; set; } = null!;
    public List<OldSavedTargetCondition> TargetConditions { get; set; } = new();
    public List<OldSavedAdditionalObjective> AdditionalObjectives { get; set; } = new();
}