namespace DataAccess.Models.EF;

public class Kill
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public string TileUrl { get; set; } = null!;
    public bool LargeWeapon { get; set; } = false;
    public bool Remote { get; set; } = false;
    public List<KillVariant> Variants { get; set; } = new();
    public KillType Type { get; set; }
}