namespace DataAccess.Models.EF;

public class Complication
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public string Description { get; set; } = null!;
    public string TileUrl { get; set; } = null!;
    public bool Global { get; set; }
    public List<ComplicationToKillType> SupportedKillTypes { get; set; } = new();
}