namespace DataAccess.Models.EF;

public class OldSavedTargetCondition
{
    public int Id { get; set; }
    public OldSavedSpin Spin { get; set; } = null!;
    public Target Target { get; set; } = null!;
    public OldSavedKillMethod KillMethod { get; set; } = null!;
    public OldSavedDisguise? Disguise { get; set; }
}