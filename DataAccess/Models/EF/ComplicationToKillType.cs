namespace DataAccess.Models.EF;

public class ComplicationToKillType
{
    public int Id { get; set; }
    public Complication Complication { get; set; } = null!;
    public KillType KillType { get; set; }
}