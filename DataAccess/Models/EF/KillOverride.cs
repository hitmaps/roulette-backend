namespace DataAccess.Models.EF;

public class KillOverride
{
    public int Id { get; set; }
    public Target Target { get; set; } = null!;
    public string Name { get; set; } = null!;
    public string TileUrl { get; set; } = null!;
}