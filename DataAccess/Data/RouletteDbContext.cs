using DataAccess.Models.EF;
using DataAccess.Models.EF.Matchups;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Data;

public class RouletteDbContext : DbContext
{
    public RouletteDbContext(DbContextOptions<RouletteDbContext> options) : base(options)
    {
        
    }

    public DbSet<Mission> Missions { get; set; }
    public DbSet<Kill> Kills { get; set; }
    public DbSet<SavedSpin> SavedSpins { get; set; }
    public DbSet<Matchup> Matchups { get; set; }
    public DbSet<Complication> Complications { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        #region Basic Tables
        modelBuilder.Entity<Mission>().ToTable("Mission");
        modelBuilder.Entity<Target>().ToTable("Target");
        modelBuilder.Entity<UniqueKill>().ToTable("UniqueKill");
        modelBuilder.Entity<BlacklistedKill>().ToTable("BlacklistedKill");
        modelBuilder.Entity<Kill>().ToTable("Kill");
        modelBuilder.Entity<Complication>().ToTable("Complication");
        modelBuilder.Entity<ComplicationToKillType>().ToTable("ComplicationToKillType");
        #endregion
        #region Saved Spins
        modelBuilder.Entity<SavedSpin>().ToTable("SavedSpin")
            .HasIndex(x => x.PublicId)
            .IsUnique();
        modelBuilder.Entity<SavedSpin>().ToTable("SavedSpin")
            .HasIndex(x => x.SpinKey)
            .IsUnique();
        modelBuilder.Entity<SavedCondition>().ToTable("SavedCondition");
        #endregion
        #region Old Saved Spins
        modelBuilder.Entity<OldSavedSpin>().ToTable("OldSavedSpin")
            .HasIndex(x => x.PublicId)
            .IsUnique();
        modelBuilder.Entity<OldSavedTargetCondition>().ToTable("OldSavedTargetCondition")
            .HasOne(x => x.Target)
            .WithMany()
            .OnDelete(DeleteBehavior.Restrict);
        modelBuilder.Entity<OldSavedKillMethod>().ToTable("OldSavedKillMethod");
        modelBuilder.Entity<OldSavedDisguise>().ToTable("OldSavedDisguise");
        modelBuilder.Entity<OldSavedAdditionalObjective>().ToTable("OldSavedAdditionalObjective")
            .HasOne(x => x.AdditionalObjective)
            .WithMany()
            .OnDelete(DeleteBehavior.Restrict);
        modelBuilder.Entity<OldSavedAdditionalObjective>().ToTable("OldSavedAdditionalObjective")
            .HasOne(x => x.Spin)
            .WithMany()
            .OnDelete(DeleteBehavior.Cascade);
        modelBuilder.Entity<OldSavedAdditionalObjectiveOption>().ToTable("OldSavedAdditionalObjectiveOption");
        #endregion
        #region Tournament Matches

        modelBuilder.Entity<Matchup>().ToTable("Matchup")
            .HasIndex(x => x.MatchupId)
            .IsUnique();
        modelBuilder.Entity<Matchup>().ToTable("Matchup")
            .HasIndex(x => x.HitmapsTournamentsId)
            .IsUnique();
        
        modelBuilder.Entity<Player>().ToTable("Player");
        #endregion
    }
}