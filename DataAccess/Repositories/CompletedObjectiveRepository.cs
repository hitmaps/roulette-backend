using Dapper;
using DataAccess.Models.Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace DataAccess.Repositories;

public class CompletedObjectiveRepository : RepositoryBase
{
    private const string InsertSql = @"INSERT INTO CompletedObjective (MatchupId, PlayerName, ObjectiveIndex) 
            VALUES (@MatchupId, @PlayerName, @ObjectiveIndex)";

    private const string DeleteSql = @"DELETE FROM CompletedObjective 
            WHERE MatchupId = @MatchupId 
              AND PlayerName = @PlayerName 
              AND ObjectiveIndex = @ObjectiveIndex";

    public CompletedObjectiveRepository(IConfiguration configuration) : base(configuration)
    {
    }

    public async Task UpdateObjectivesAsync(List<CompletedObjective> completedObjectives)
    {
        await using var connection = GetConnection();
        connection.Open();

        await connection.ExecuteAsync(InsertSql, completedObjectives.Where(x => x.Completed).ToList());
        await connection.ExecuteAsync(DeleteSql, completedObjectives.Where(x => !x.Completed).ToList());
    }
}