using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace DataAccess.Repositories;

public abstract class RepositoryBase
{
    private readonly IConfiguration configuration;

    protected RepositoryBase(IConfiguration configuration)
    {
        this.configuration = configuration;
    }

    internal SqlConnection GetConnection()
    {
        return new SqlConnection(configuration.GetConnectionString("Roulette"));
    }
}