using Dapper;
using DataAccess.Models.Dapper;
using Microsoft.Extensions.Configuration;

namespace DataAccess.Repositories;

public class MatchupRepository : RepositoryBase
{
    public MatchupRepository(IConfiguration configuration) : base(configuration)
    {
    }

    public Matchup? GetForMatchupId(Guid matchupId)
    {
        const string sql = "SELECT * FROM Matchup WHERE MatchupId = @MatchupId";
        using var connection = GetConnection();
        connection.Open();

        return connection.QueryFirstOrDefault<Matchup>(sql, new
        {
            MatchupId = matchupId
        });
    }
}