using Microsoft.AspNetCore.Mvc;
using RouletteBackend.BusinessLogic;
using RouletteBackend.Controllers.ViewModels;

namespace RouletteBackend.Controllers;

public class MatchHistoryController : ApiControllerBase
{
    private readonly IMatchupService matchupService;

    public MatchHistoryController(IMatchupService matchupService)
    {
        this.matchupService = matchupService;
    }

    [HttpGet("match-history")]
    public IActionResult GetMatchHistoryForMatches(string matchIds)
    {
        var splitMatchIds = matchIds.Split(',').Select(Guid.Parse).ToList();
        var completedMatches = matchupService.GetCompletedMatchupsForIds(splitMatchIds);

        return Ok(new
        {
            Matches = completedMatches.Select(MatchupForStatsViewModel.FromDbModel).ToList()
        });
    }
}