using DataAccess.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RouletteBackend.BusinessLogic.HitmapsDotCom;
using RouletteBackend.Controllers.ViewModels;

namespace RouletteBackend.Controllers.Internal;

//[ApiExplorerSettings(IgnoreApi = true)]
public class HomePageController : ApiControllerBase
{
    private readonly RouletteDbContext dbContext;
    private readonly IHitmapsDotComService hitmapsDotComService;

    public HomePageController(RouletteDbContext dbContext, IHitmapsDotComService hitmapsDotComService)
    {
        this.dbContext = dbContext;
        this.hitmapsDotComService = hitmapsDotComService;
    }

    [HttpGet("internal/home")]
    public async Task<IActionResult> GetHomepageData()
    {
        return Ok(new HomePageDataViewModel
        {
            Missions = dbContext.Missions.Include(x => x.Targets).Select(MissionViewModel.FromDbModel).ToList(),
            HitmapsMissions = await hitmapsDotComService.GetHitmapsLocationsAsync()
        });
    }
}

public class HomePageDataViewModel
{
    public List<MissionViewModel> Missions { get; set; } = new();
    public List<HitmapsLocationModel> HitmapsMissions { get; set; } = new();
}