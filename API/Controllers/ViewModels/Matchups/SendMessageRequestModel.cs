namespace RouletteBackend.Controllers.ViewModels.Matchups;

public class SendMessageRequestModel
{
    public string PlayerPublicId { get; set; } = null!;
    public string Message { get; set; } = null!;
}