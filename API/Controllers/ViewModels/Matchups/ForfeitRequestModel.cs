namespace RouletteBackend.Controllers.ViewModels.Matchups;

public class ForfeitRequestModel
{
    public bool Forfeit { get; set; }
}