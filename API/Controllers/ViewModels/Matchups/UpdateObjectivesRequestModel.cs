using DataAccess.Models.Dapper;

namespace RouletteBackend.Controllers.ViewModels.Matchups;

public class UpdateObjectivesRequestModel
{
    public string PlayerName { get; set; } = null!;
    public int ObjectiveIndex { get; set; }
    public bool Completed { get; set; }

    public CompletedObjective ToDbCompletedObjective(int matchupId)
    {
        return new CompletedObjective
        {
            MatchupId = matchupId,
            PlayerName = PlayerName,
            ObjectiveIndex = ObjectiveIndex,
            Completed = Completed
        };
    }
}