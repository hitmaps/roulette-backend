namespace RouletteBackend.Controllers.ViewModels.Matchups;

public class SendSpinRequestModel
{
    public DateTime StartTime { get; set; }
    public int? SpinDuration { get; set; }
    public SpinResultViewModel Spin { get; set; } = null!;
}