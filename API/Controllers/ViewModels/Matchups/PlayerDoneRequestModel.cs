namespace RouletteBackend.Controllers.ViewModels.Matchups;

public class PlayerDoneRequestModel
{
    public string PlayerPublicId { get; set; } = null!;
    public Guid TimerFingerprint { get; set; }
}