namespace RouletteBackend.Controllers.ViewModels.Matchups;

public class ParticipantHandleMessageRequestModel
{
    public int Id { get; set; }
    public string Action { get; set; }
}