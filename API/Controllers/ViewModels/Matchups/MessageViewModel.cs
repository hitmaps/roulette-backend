using DataAccess.Models.EF.Matchups;
using RouletteBackend.BusinessLogic.Extensions;

namespace RouletteBackend.Controllers.ViewModels.Matchups;

public class MessageViewModel
{
    public int Id { get; set; }
    public string PlayerPublicId { get; set; } = null!;
    public string SentAt { get; set; } = null!;
    public string Content { get; set; } = null!;
    public string? ReceivedAt { get; set; }
    public string? AcknowledgedAt { get; set; }

    public static MessageViewModel FromDbModel(Message message)
    {
        return new MessageViewModel
        {
            Id = message.Id,
            PlayerPublicId = message.Player.PublicId,
            SentAt = message.SentAt.ToApiDateFormat(),
            Content = message.Content,
            ReceivedAt = message.ReceivedAt?.ToApiDateFormat(),
            AcknowledgedAt = message.AcknowledgedAt?.ToApiDateFormat()
        };
    }
}