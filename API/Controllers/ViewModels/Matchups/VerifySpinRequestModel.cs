namespace RouletteBackend.Controllers.ViewModels.Matchups;

public class VerifySpinRequestModel
{
    public int MapSelectionId { get; set; }
    public string? PlayerPublicId { get; set; }
    public string VerifyResult { get; set; } = null!;
}