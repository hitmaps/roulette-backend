using DataAccess.Models.Dapper;

namespace RouletteBackend.Controllers.ViewModels.Matchups;

public class UpdateObjectivesRequestModelCollection
{
    public List<UpdateObjectivesRequestModel> Objectives { get; set; } = new();

    public List<CompletedObjective> ToDbCompletedObjectives(int matchupId)
    {
        return Objectives
            .Select(x => x.ToDbCompletedObjective(matchupId))
            .ToList();
    }
}