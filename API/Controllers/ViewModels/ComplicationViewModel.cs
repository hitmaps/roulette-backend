using RouletteBackend.BusinessLogic;

namespace RouletteBackend.Controllers.ViewModels;

public class ComplicationViewModel
{
    public string Name { get; set; } = null!;
    public string Description { get; set; } = null!;
    public string TileUrl { get; set; } = null!;

    public static ComplicationViewModel FromComplication(Complication complication)
    {
        return new ComplicationViewModel
        {
            Name = complication.Name,
            Description = complication.Description,
            TileUrl = complication.TileUrl
        };
    }
}