using System.Text.Json;
using DataAccess.Models;

namespace RouletteBackend.Controllers.ViewModels;

public class TargetConditionsViewModel
{
    public TargetViewModel Target { get; set; } = null!;
    public KillMethodViewModel? KillMethod { get; set; }
    public DisguiseViewModel? Disguise { get; set; }
    public List<ComplicationViewModel>? Complications { get; set; } = new();

    public string Serialized()
    {
        var killMethod = KillMethod != null ? $"{KillMethod.Name}%%{KillMethod.SelectedVariant}" : "ANY";
        var disguise = Disguise != null && Disguise.Name != "Any Disguise" ? Disguise.Name : "ANY";
        var complications = Complications == null ? 
            new List<string>() : 
            Complications.Select(x => x.Name).ToList();

        return JsonSerializer.Serialize(new
        {
            TargetName = Target.Name,
            KillMethod = killMethod,
            Disguise = disguise,
            Complications = complications
        });
    }
}