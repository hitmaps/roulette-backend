namespace RouletteBackend.Controllers.ViewModels;

public class CreateMatchupRequestModel
{
    public int? HitmapsTournamentsMatchId { get; set; }
    public List<PlayerViewModel> Players { get; set; } = new();
    public List<MapSelectionViewModel> MapSelections { get; set; } = new();
    public string? OverlayTheme { get; set; }
    public int? PointsForVictory { get; set; }
}