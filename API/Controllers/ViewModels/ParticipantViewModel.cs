using DataAccess.Models.EF.Matchups;
using RouletteBackend.BusinessLogic.Extensions;

namespace RouletteBackend.Controllers.ViewModels;

public class ParticipantViewModel
{
    public string PublicId { get; set; } = null!;
    public string Name { get; set; } = null!;
    public string? AvatarUrl { get; set; }
    public string LastPing { get; set; } = null!;
    public string? CompleteTime { get; set; }
    public bool Forfeit { get; set; }

    public static ParticipantViewModel FromDbModel(Player player)
    {
        return new ParticipantViewModel
        {
            PublicId = player.PublicId,
            Name = player.Name,
            AvatarUrl = player.AvatarUrl,
            LastPing = player.LastPing.ToApiDateFormat(),
            CompleteTime = player.CompleteTime?.ToApiDateFormat(),
            Forfeit = player.Forfeit
        };
    }
}