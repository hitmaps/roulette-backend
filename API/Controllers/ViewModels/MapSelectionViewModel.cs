using DataAccess.Models.EF.Matchups;

namespace RouletteBackend.Controllers.ViewModels;

public class MapSelectionViewModel
{
    public int Id { get; set; }
    public string HitmapsSlug { get; set; } = null!;
    public string? ChosenByName { get; set; }
    public bool Complete { get; set; }
    public string? WinnerName { get; set; }

    public static MapSelectionViewModel FromDbModel(MapSelection mapSelection)
    {
        return new MapSelectionViewModel
        {
            Id = mapSelection.Id,
            HitmapsSlug = mapSelection.Mission.Slug,
            ChosenByName = mapSelection.ChosenByName,
            Complete = mapSelection.Complete,
            WinnerName = mapSelection.WinnerName
        };
    }
}