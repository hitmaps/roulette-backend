using DataAccess.Models.EF.Matchups;

namespace RouletteBackend.Controllers.ViewModels;

public class MatchupTimerViewModel
{
    public Guid Fingerprint { get; set; }
    public bool Paused { get; set; }
    public int? MatchDurationInMinutes { get; set; }
    public string? StartTime { get; set; }
    public string? EndTime { get; set; }
    
    public string? ServerTime { get; set; }

    public static MatchupTimerViewModel FromDbModel(Matchup matchup)
    {
        return new MatchupTimerViewModel
        {
            Fingerprint = matchup.TimerFingerprint,
            Paused = matchup.TimerPaused,
            MatchDurationInMinutes = matchup.TimerEndTime == null ? 
                null : 
                (int?)matchup.TimerEndTime?.Subtract(matchup.TimerStartTime!.Value).TotalMinutes,
            StartTime = matchup.TimerStartTime?.ToString("yyyy-MM-ddTHH:mm:ssZ"),
            EndTime = matchup.TimerEndTime?.ToString("yyyy-MM-ddTHH:mm:ssZ"),
            ServerTime = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ")
        };
    }
}