using DataAccess.Models.EF;
using RouletteBackend.BusinessLogic;

namespace RouletteBackend.Controllers.ViewModels;

public class MissionViewModel
{
    // Only nullable for frontend send spin requests
    public string? Name { get; set; }
    public string Slug { get; set; } = null!;
    public string? VariantSlug { get; set; }
    public int PublicIdPrefix { get; set; }
    public List<TargetViewModel> Targets { get; set; } = new();

    public static MissionViewModel FromDbModel(Mission mission)
    {
        var missionName = HitmapsMissionNameCache.MissionSlugToName.GetValueOrDefault(mission.Slug, string.Empty); 
        if (mission.VariantSlug != "professional" && mission.VariantSlug != "standard")
        {
            missionName = HitmapsMissionNameCache.MissionSlugToName.GetValueOrDefault($"{mission.Slug}|{mission.VariantSlug}", string.Empty);
        }
        
        return new MissionViewModel
        {
            Name = missionName,
            Slug = mission.Slug,
            VariantSlug = mission.VariantSlug,
            PublicIdPrefix = mission.PublicIdPrefix,
            Targets = mission.Targets.Select(TargetViewModel.FromDbModel).ToList()
        };
    }
}