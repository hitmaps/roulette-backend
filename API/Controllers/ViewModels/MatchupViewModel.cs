using DataAccess.Models.EF.Matchups;

namespace RouletteBackend.Controllers.ViewModels;

public class MatchupViewModel
{
    public Guid MatchupId { get; set; }
    public int? HitmapsTournamentId { get; set; }
    public List<ParticipantViewModel> Participants { get; set; } = new();
    public List<MapSelectionViewModel> MapSelections { get; set; } = new();
    public MatchupTimerViewModel Timer { get; set; } = null!;
    public SpinResultViewModel? CurrentSpin { get; set; }
    public string? OverlayTheme { get; set; }
    public int? PointsForVictory { get; set; }
    public List<CompletedObjectiveViewModel> CompletedObjectives { get; set; } = new();

    public static MatchupViewModel FromDbModel(Matchup matchup)
    {
        return new MatchupViewModel
        {
            MatchupId = matchup.MatchupId,
            HitmapsTournamentId = matchup.HitmapsTournamentsId,
            Participants = matchup.Players.Select(ParticipantViewModel.FromDbModel).ToList(),
            MapSelections = matchup.MapSelections.Select(MapSelectionViewModel.FromDbModel).ToList(),
            Timer = MatchupTimerViewModel.FromDbModel(matchup),
            CurrentSpin = SpinResultViewModel.FromSavedSpin(matchup.CurrentSpin),
            OverlayTheme = matchup.OverlayTheme,
            PointsForVictory = matchup.PointsForVictory,
            CompletedObjectives = matchup.CompletedObjectives.Select(CompletedObjectiveViewModel.FromDbModel).ToList()
        };
    }
}