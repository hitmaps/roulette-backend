using DataAccess.Models.EF.Matchups;
using RouletteBackend.BusinessLogic.Extensions;

namespace RouletteBackend.Controllers.ViewModels;

public class MapSelectionForStatsViewModel
{
    public int Id { get; set; }
    public string HitmapsSlug { get; set; } = null!;
    public string? ChosenByName { get; set; }
    public bool Complete { get; set; }
    public string? WinnerName { get; set; }
    public SpinResultViewModel? Spin { get; set; }
    public string? MapStartedAt { get; set; }
    public string? WinnerFinishedAt { get; set; }
    public string? ResultVerifiedAt { get; set; }

    public static MapSelectionForStatsViewModel FromDbModel(MapSelection mapSelection)
    {
        return new MapSelectionForStatsViewModel
        {
            Id = mapSelection.Id,
            HitmapsSlug = mapSelection.Mission.Slug,
            ChosenByName = mapSelection.ChosenByName,
            Complete = mapSelection.Complete,
            WinnerName = mapSelection.WinnerName,
            Spin = SpinResultViewModel.FromSavedSpin(mapSelection.SavedSpin),
            MapStartedAt = mapSelection.MapStartedAt.ToNullSafeApiDateFormat(),
            WinnerFinishedAt = mapSelection.WinnerFinishedAt.ToNullSafeApiDateFormat(),
            ResultVerifiedAt = mapSelection.ResultVerifiedAt.ToNullSafeApiDateFormat()
        };
    }
}