using DataAccess.Models.EF;

namespace RouletteBackend.Controllers.ViewModels;

public class CompleteMethodViewModel
{
    public string Name { get; set; } = null!;
    public string TileUrl { get; set; } = null!;

    public static CompleteMethodViewModel FromDbModel(AdditionalObjectiveOption option)
    {
        return new CompleteMethodViewModel
        {
            Name = option.Name,
            TileUrl = option.TileUrl
        };
    }

    public static CompleteMethodViewModel FromSavedAdditionalObjectiveOption(OldSavedAdditionalObjectiveOption option)
    {
        return new CompleteMethodViewModel
        {
            Name = option.Name,
            TileUrl = option.TileUrl
        };
    }
}