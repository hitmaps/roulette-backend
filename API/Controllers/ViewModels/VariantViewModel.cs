using RouletteBackend.BusinessLogic.KillMethods;

namespace RouletteBackend.Controllers.ViewModels;

public class VariantViewModel
{
    public string Name { get; set; } = null!;
    public bool LargeWeapon { get; set; }
    public string? ImageOverride { get; set; }

    public static VariantViewModel FromVariant(Variant variant)
    {
        return new VariantViewModel
        {
            Name = variant.Name,
            LargeWeapon = variant.LargeWeapon,
            ImageOverride = variant.ImageOverride
        };
    }
}