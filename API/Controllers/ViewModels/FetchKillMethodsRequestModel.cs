using RouletteBackend.ViewModels;

namespace RouletteBackend.Controllers.ViewModels;

public class FetchKillMethodsRequestModel : CriteriaFilterViewModel
{
    public string MissionGame { get; set; } = null!;
    public string MissionLocation { get; set; } = null!;
    public string MissionSlug { get; set; } = null!;
    public string? MissionVariant { get; set; }
    public string TargetName { get; set; } = null!;
}