using DataAccess.Models.EF;

namespace RouletteBackend.Controllers.ViewModels;

public class ObjectiveViewModel
{
    public string Name { get; set; } = null!;
    public string TileUrl { get; set; } = null!;

    public static ObjectiveViewModel FromDbModel(AdditionalObjective objective)
    {
        return new ObjectiveViewModel
        {
            Name = objective.Name,
            TileUrl = objective.TileUrl
        };
    }
}