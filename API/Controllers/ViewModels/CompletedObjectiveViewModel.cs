using DataAccess.Models.EF.Matchups;

namespace RouletteBackend.Controllers.ViewModels;

public class CompletedObjectiveViewModel
{
    public int Id { get; set; }
    public string PlayerName { get; set; } = null!;
    public int ObjectiveIndex { get; set; }

    public static CompletedObjectiveViewModel FromDbModel(CompletedObjective objective)
    {
        return new CompletedObjectiveViewModel
        {
            Id = objective.Id,
            PlayerName = objective.PlayerName,
            ObjectiveIndex = objective.ObjectiveIndex
        };
    }
}