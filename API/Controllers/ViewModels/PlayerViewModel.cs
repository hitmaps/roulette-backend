namespace RouletteBackend.Controllers.ViewModels;

public class PlayerViewModel
{
    public string Name { get; set; } = null!;
    public string? AvatarUrl { get; set; }
}