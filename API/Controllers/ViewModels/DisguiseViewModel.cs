using DataAccess.Models.EF;
using RouletteBackend.BusinessLogic;

namespace RouletteBackend.Controllers.ViewModels;

public class DisguiseViewModel
{
    public string Name { get; set; } = null!;
    public string? TileUrl { get; set; }

    public static DisguiseViewModel FromDisguise(Disguise disguise)
    {
        return new DisguiseViewModel
        {
            Name = disguise.Name,
            TileUrl = disguise.TileUrl
        };
    }

    public static DisguiseViewModel? FromSavedDisguise(OldSavedDisguise? disguise)
    {
        if (disguise == null)
        {
            return new DisguiseViewModel
            {
                Name = "Any Disguise",
                TileUrl = "https://media.hitmaps.com/img/hitman3/ui/tiles/any_disguise.jpg"
            };
        }
        
        return new DisguiseViewModel
        {
            Name = disguise.Name,
            TileUrl = disguise.TileUrl
        };
    }
}