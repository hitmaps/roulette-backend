using DataAccess.Models.EF.Matchups;

namespace RouletteBackend.Controllers.ViewModels;

public class MatchupForStatsViewModel
{
    public Guid MatchupId { get; set; }
    public List<ParticipantViewModel> Participants { get; set; } = new();
    public List<MapSelectionForStatsViewModel> MapSelections { get; set; } = new();

    public static MatchupForStatsViewModel FromDbModel(Matchup matchup)
    {
        return new MatchupForStatsViewModel
        {
            MatchupId = matchup.MatchupId,
            Participants = matchup.Players.Select(ParticipantViewModel.FromDbModel).ToList(),
            MapSelections = matchup.MapSelections.Select(MapSelectionForStatsViewModel.FromDbModel).ToList()
        };
    }
}