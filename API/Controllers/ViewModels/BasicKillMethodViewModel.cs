using RouletteBackend.BusinessLogic.KillMethods;

namespace RouletteBackend.Controllers.ViewModels;

public class BasicKillMethodViewModel
{
    public string Name { get; set; } = null!;
    public bool LargeWeapon { get; set; }
    public string TileUrl { get; set; } = null!;
    public List<VariantViewModel> Variants { get; set; } = new();
    public bool Remote { get; set; }

    public static BasicKillMethodViewModel FromBasicKillMethod(BasicKillMethod method)
    {
        return new BasicKillMethodViewModel
        {
            Name = method.Name,
            LargeWeapon = method.LargeWeapon,
            TileUrl = method.TileUrl,
            Variants = method.Variants.Select(VariantViewModel.FromVariant).ToList(),
            Remote = method.Remote
        };
    }
}