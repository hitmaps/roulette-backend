using System.Text.Json;
using DataAccess.Models.EF;

namespace RouletteBackend.Controllers.ViewModels;

public class ObjectiveConditionsViewModel
{
    public ObjectiveViewModel Objective { get; set; } = null!;
    public CompleteMethodViewModel? CompletionMethod { get; set; }
    public DisguiseViewModel? Disguise { get; set; }
    public List<ComplicationViewModel> Complications { get; set; } = new();

    public static ObjectiveConditionsViewModel FromSavedAdditionalObjective(OldSavedAdditionalObjective objective)
    {
        return new ObjectiveConditionsViewModel
        {
            Objective = ObjectiveViewModel.FromDbModel(objective.AdditionalObjective),
            CompletionMethod = CompleteMethodViewModel.FromSavedAdditionalObjectiveOption(objective.CompletionMethod),
            Disguise = DisguiseViewModel.FromSavedDisguise(objective.Disguise)
        };
    }

    public string Serialized()
    {
        var objective = Objective.Name;
        var completeMethod = CompletionMethod != null ? CompletionMethod.Name : "ANY";
        var disguise = Disguise != null && Disguise.Name != "Any Disguise" ? Disguise.Name : "ANY";

        return JsonSerializer.Serialize(new
        {
            Objective = objective,
            CompletionMethod = completeMethod,
            Disguise = disguise
        });
    }
}