using DataAccess.Models.EF;

namespace RouletteBackend.Controllers.ViewModels;

public class TargetViewModel
{
    public string Name { get; set; } = null!;
    public string? TileUrl { get; set; }

    public static TargetViewModel FromDbModel(Target target)
    {
        return new TargetViewModel
        {
            Name = target.Name,
            TileUrl = target.TileUrl
        };
    }
}