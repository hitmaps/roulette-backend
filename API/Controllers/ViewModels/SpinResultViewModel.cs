using System.Text.Json;
using DataAccess.Models.EF;

namespace RouletteBackend.Controllers.ViewModels;

public class SpinResultViewModel
{
    public MissionViewModel Mission { get; set; } = null!;
    public List<TargetConditionsViewModel> TargetConditions { get; set; } = new();
    public List<ObjectiveConditionsViewModel> AdditionalObjectives { get; set; } = new();

    public static SpinResultViewModel? FromSavedSpin(SavedSpin? savedSpin)
    {
        if (savedSpin == null)
        {
            return null;
        }

        var targetConditions = new List<TargetConditionsViewModel>();
        var additionalObjectiveModels = new List<ObjectiveConditionsViewModel>();

        var targets = savedSpin.Mission.Targets;
        var additionalObjectives = savedSpin.Mission.AdditionalObjectives;
        foreach (var savedCondition in savedSpin.SavedConditions)
        {
            var savedConditionType = SavedConditionType.FromValue(savedCondition.ConditionType);

            if (savedConditionType.Equals(SavedConditionType.TargetKill))
            {
                TransformTargetKill(targets, savedCondition, targetConditions);
            } else if (savedConditionType.Equals(SavedConditionType.TargetDisguise))
            {
                TransformTargetDisguise(targets, savedCondition, targetConditions);
            } else if (savedConditionType.Equals(SavedConditionType.TargetComplication))
            {
                TransformTargetComplication(targets, savedCondition, targetConditions);
            } else if (savedConditionType.Equals(SavedConditionType.AdditionalObjectiveCompletionMethod))
            {
                TransformAdditionalObjectiveCompletionMethod(additionalObjectives, savedCondition, additionalObjectiveModels);
            } else if (savedConditionType.Equals(SavedConditionType.AdditionalObjectiveDisguise))
            {
                TransformAdditionalObjectiveDisguise(additionalObjectives, savedCondition, additionalObjectiveModels);
            }
        }

        return new SpinResultViewModel
        {
            Mission = MissionViewModel.FromDbModel(savedSpin.Mission),
            TargetConditions = targetConditions,
            AdditionalObjectives = additionalObjectiveModels
        };
    }

    private static void TransformTargetKill(IEnumerable<Target> targets, 
        SavedCondition savedCondition, 
        ICollection<TargetConditionsViewModel> targetConditions)
    {
        var target = targets.First(x => x.Id == savedCondition.EntityId);
        var targetCondition = GetTargetCondition(targetConditions, target);

        targetCondition.KillMethod = JsonSerializer.Deserialize<KillMethodViewModel>(savedCondition.ConditionValue);
    }

    private static TargetConditionsViewModel GetTargetCondition(ICollection<TargetConditionsViewModel> targetConditions, 
        Target target)
    {
        var targetCondition = targetConditions.FirstOrDefault(x => x.Target.Name == target.Name);
        // ReSharper disable once InvertIf
        if (targetCondition == null)
        {
            targetCondition = new TargetConditionsViewModel
            {
                Target = TargetViewModel.FromDbModel(target)
            };
            targetConditions.Add(targetCondition);
        }
        
        return targetCondition;
    }

    private static void TransformTargetDisguise(IEnumerable<Target> targets, 
        SavedCondition savedCondition, 
        ICollection<TargetConditionsViewModel> targetConditions)
    {
        var target = targets.First(x => x.Id == savedCondition.EntityId);
        var targetCondition = GetTargetCondition(targetConditions, target);

        targetCondition.Disguise = JsonSerializer.Deserialize<DisguiseViewModel>(savedCondition.ConditionValue);
    }

    private static void TransformTargetComplication(IEnumerable<Target> targets, 
        SavedCondition savedCondition, 
        ICollection<TargetConditionsViewModel> targetConditions)
    {
        var target = targets.First(x => x.Id == savedCondition.EntityId);
        var targetCondition = GetTargetCondition(targetConditions, target);

        targetCondition.Complications!.Add(JsonSerializer.Deserialize<ComplicationViewModel>(savedCondition.ConditionValue)!);
    }

    private static void TransformAdditionalObjectiveCompletionMethod(IEnumerable<AdditionalObjective> additionalObjectives, 
        SavedCondition savedCondition, 
        ICollection<ObjectiveConditionsViewModel> additionalObjectiveModels)
    {
        var additionalObjective = additionalObjectives.First(x => x.Id == savedCondition.EntityId);
        var additionalObjectiveModel = GetAdditionalObjectiveModel(additionalObjectiveModels, additionalObjective);

        additionalObjectiveModel.CompletionMethod =
            JsonSerializer.Deserialize<CompleteMethodViewModel>(savedCondition.ConditionValue);
    }

    private static ObjectiveConditionsViewModel GetAdditionalObjectiveModel(ICollection<ObjectiveConditionsViewModel> additionalObjectiveModels, 
        AdditionalObjective additionalObjective)
    {
        var additionalObjectiveModel = additionalObjectiveModels.FirstOrDefault(x => x.Objective.Name == additionalObjective.Name);
        // ReSharper disable once InvertIf
        if (additionalObjectiveModel == null)
        {
            additionalObjectiveModel = new ObjectiveConditionsViewModel
            {
                Objective = ObjectiveViewModel.FromDbModel(additionalObjective)
            };
            additionalObjectiveModels.Add(additionalObjectiveModel);
        }
        
        return additionalObjectiveModel;
    }

    private static void TransformAdditionalObjectiveDisguise(IEnumerable<AdditionalObjective> additionalObjectives, 
        SavedCondition savedCondition, 
        ICollection<ObjectiveConditionsViewModel> additionalObjectiveModels)
    {
        var additionalObjective = additionalObjectives.First(x => x.Id == savedCondition.EntityId);
        var additionalObjectiveModel = GetAdditionalObjectiveModel(additionalObjectiveModels, additionalObjective);

        additionalObjectiveModel.Disguise =
            JsonSerializer.Deserialize<DisguiseViewModel>(savedCondition.ConditionValue);
    }
}