using DataAccess.Models.EF;
using RouletteBackend.BusinessLogic.KillMethods;

namespace RouletteBackend.Controllers.ViewModels;

public class KillMethodViewModel
{
    public string Name { get; set; } = null!;
    public string? TileUrl { get; set; }
    public string? SelectedVariant { get; set; }
    public string KillType { get; set; } = null!;

    public static KillMethodViewModel FromGeneratedSpin(BasicKillMethod killMethod)
    {
        var killVariant = killMethod.Variants.FirstOrDefault(y => y.Chosen);
        return new KillMethodViewModel
        {
            Name = killMethod.Name,
            TileUrl = killVariant?.ImageOverride ?? killMethod.TileUrl,
            SelectedVariant = killVariant?.Name,
            KillType = killMethod.KillType.ToString()
        };
    }

    // TODO Potentially remove
    public static KillMethodViewModel? FromSavedKillMethod(OldSavedKillMethod killMethod)
    {
        return new KillMethodViewModel
        {
            Name = killMethod.Name,
            TileUrl = killMethod.TileUrl,
            SelectedVariant = killMethod.SelectedVariant,
            KillType = "Unknown"
        };
    }
}