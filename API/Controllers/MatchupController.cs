using Microsoft.AspNetCore.Mvc;
using RouletteBackend.BusinessLogic;
using RouletteBackend.BusinessLogic.Discord;
using RouletteBackend.BusinessLogic.Exceptions;
using RouletteBackend.Controllers.ViewModels;
using RouletteBackend.Controllers.ViewModels.Matchups;

namespace RouletteBackend.Controllers;

public class MatchupController : ApiControllerBase
{
    private readonly IMatchupService matchupService;
    private readonly IMatchHubService matchHubService;
    private readonly IMessageService messageService;

    public MatchupController(IMatchupService matchupService, 
        IMatchHubService matchHubService, 
        IMessageService messageService)
    {
        this.matchupService = matchupService;
        this.matchHubService = matchHubService;
        this.messageService = messageService;
    }

    [HttpGet("matchups/{matchupId:guid}")]
    public IActionResult GetMatchup(Guid matchupId, [FromQuery] string? playerId)
    {
        var matchup = matchupService.GetMatchup(matchupId, playerId);

        if (matchup == null)
        {
            return NotFound();
        }

        return Ok(MatchupViewModel.FromDbModel(matchup));
    }

    [HttpPost("matchups")]
    [DiscordAuth(Required = false)]
    public async Task<IActionResult> CreateMatchup([FromBody] CreateMatchupRequestModel requestModel)
    {
        return Ok(MatchupViewModel.FromDbModel(await matchupService.CreateMatchupAsync(requestModel, GetDiscordUserContext())));
    }

    private DiscordUserContext? GetDiscordUserContext()
    {
        return (DiscordUserContext?)RouteData.Values["userContext"];
    }

    [HttpPut("matchups/{matchupId:guid}/send-spin")]
    [DiscordAuth(Required = false)]
    public async Task<IActionResult> SendSpin(Guid matchupId, [FromBody] SendSpinRequestModel requestModel)
    {
        // TODO move into service
        var matchup = matchupService.GetMatchup(matchupId);

        if (matchup == null)
        {
            return NotFound();
        }

        matchup = await matchupService.SendSpinAsync(matchupId, requestModel, GetDiscordUserContext());

        var matchupViewModel = MatchupViewModel.FromDbModel(matchup);
        await matchHubService.SendUpdatedMatchAsync(matchupViewModel);
        return Ok(matchupViewModel);
    }

    [HttpPut("matchups/{matchupId:guid}/clear-spin")]
    [DiscordAuth(Required = false)]
    public async Task<IActionResult> ClearSpin(Guid matchupId)
    {
        // TODO move into service
        var matchup = matchupService.GetMatchup(matchupId);

        if (matchup == null)
        {
            return NotFound();
        }

        matchup = await matchupService.ClearSpinAsync(matchupId, GetDiscordUserContext());

        var matchupViewModel = MatchupViewModel.FromDbModel(matchup);
        await matchHubService.SendUpdatedMatchAsync(matchupViewModel);
        return Ok(matchupViewModel);
    }

    [HttpPatch("matchups/{matchupId:guid}/done")]
    public async Task<IActionResult> PlayerDone(Guid matchupId, [FromBody] PlayerDoneRequestModel requestModel)
    {
        //-- We get the completion time here so that any database delays don't negatively impact the player
        var completionTime = DateTime.UtcNow;
        // TODO move into service
        var matchup = matchupService.GetMatchup(matchupId);

        if (matchup == null)
        {
            return NotFound();
        }

        try
        {
            var player = matchupService.MarkPlayerAsComplete(matchupId, requestModel, completionTime);
            await matchHubService.SendPlayerDoneAsync(matchupId, ParticipantViewModel.FromDbModel(player));
            return NoContent();
        } catch (Exception e) when (e is DoneButtonAlreadyClickedException or ConflictException<Guid>)
        {
            return Conflict(new
            {
                e.Message
            });
        }
    }

    [HttpPatch("matchups/{matchupId:guid}/verify")]
    [DiscordAuth(Required = false)]
    public async Task<IActionResult> VerifySpin(Guid matchupId, [FromBody] VerifySpinRequestModel requestModel)
    {
        // TODO move into service
        var matchup = matchupService.GetMatchup(matchupId);

        if (matchup == null)
        {
            return NotFound();
        }

        matchup = await matchupService.VerifySpinAsync(matchupId, requestModel, GetDiscordUserContext());

        var matchupViewModel = MatchupViewModel.FromDbModel(matchup);
        await matchHubService.SendUpdatedMatchAsync(matchupViewModel);
        return Ok(matchupViewModel);
    }

    [HttpDelete("matchups/{matchupId:guid}/map-result/{mapSelectionId:int}")]
    [DiscordAuth(Required = false)]
    public async Task<IActionResult> RemoveMostRecentResult(Guid matchupId, int mapSelectionId)
    {
        // TODO move into service
        var matchup = matchupService.GetMatchup(matchupId);

        if (matchup == null)
        {
            return NotFound();
        }

        try
        {
            matchup = await matchupService.DeleteMostRecentResult(matchupId, mapSelectionId, GetDiscordUserContext());
        }
        catch (ConflictException<int> e)
        {
            return Conflict(new
            {
                e.Message
            });
        }

        var matchupViewModel = MatchupViewModel.FromDbModel(matchup);
        await matchHubService.SendUpdatedMatchAsync(matchupViewModel);
        return Ok(matchupViewModel);
    }

    [HttpGet("matchups/{matchupId:guid}/players/{playerPublicId}/unread-message")]
    public IActionResult GetUnacknowledgedMessage(Guid matchupId, string playerPublicId)
    {
        var message = messageService.GetUnacknowledgedMessage(matchupId, playerPublicId);

        if (message == null)
        {
            return NoContent();
        }

        return Ok(MessageViewModel.FromDbModel(message));
    }

    [HttpPost("matchups/{matchupId:guid}/messages")]
    [DiscordAuth(Required = false)]
    public async Task<IActionResult> SendMessage(Guid matchupId, [FromBody] SendMessageRequestModel requestModel)
    {
        var message = await messageService.SendMessageAsync(matchupId, requestModel, GetDiscordUserContext());

        var messageViewModel = MessageViewModel.FromDbModel(message);
        await matchHubService.SendNewMessageAsync(matchupId, messageViewModel);
        return Ok(messageViewModel);
    }

    [HttpPatch("matchups/{matchupId:guid}/players/{playerPublicId}/messages")]
    public async Task<IActionResult> ParticipantHandleMessage(Guid matchupId, string playerPublicId,
        [FromBody] ParticipantHandleMessageRequestModel requestModel)
    {
        var message = messageService.ParticipantHandleMessage(matchupId, playerPublicId, requestModel);

        var messageViewModel = MessageViewModel.FromDbModel(message);

        if (requestModel.Action == "Acknowledge")
        {
            await matchHubService.SendMessageAcknowledgedAsync(matchupId, messageViewModel);
        }

        return Ok(messageViewModel);
    }

    [HttpPut("matchups/{matchupId:guid}/players/{forfeitPlayerId}/forfeit")]
    [DiscordAuth(Required = false)]
    public async Task<IActionResult> DeclareForfeit(Guid matchupId, string forfeitPlayerId, [FromBody] ForfeitRequestModel requestModel)
    {
        var matchup = await matchupService.UpdateForfeitAsync(matchupId, forfeitPlayerId, requestModel, GetDiscordUserContext());

        var matchupViewModel = MatchupViewModel.FromDbModel(matchup);

        await matchHubService.SendUpdatedMatchAsync(matchupViewModel);
        return Ok(matchupViewModel);
    }

    [HttpPatch("matchups/{matchupId:guid}/objectives")]
    [DiscordAuth(Required = false)]
    public async Task<IActionResult> UpdateObjective(Guid matchupId,
        [FromBody] UpdateObjectivesRequestModelCollection requestModel)
    {
        await matchupService.UpdateObjectivesAsync(matchupId, requestModel, GetDiscordUserContext());

        await matchHubService.SendObjectiveUpdateAsync(matchupId, requestModel);
        return Ok(requestModel);
    }
}