using Microsoft.AspNetCore.Mvc;

namespace RouletteBackend.Controllers;

public class ResultWithBody : JsonResult
{
    public ResultWithBody(int statusCode, string message) : base(new { message })
    {
        StatusCode = statusCode;
    }
}