using DataAccess.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RouletteBackend.Controllers.ViewModels;

namespace RouletteBackend.Controllers;

public class MissionController : ApiControllerBase
{
    private readonly RouletteDbContext dbContext;

    public MissionController(RouletteDbContext dbContext)
    {
        this.dbContext = dbContext;
    }

    [HttpGet("missions")]
    public IActionResult GetSupportedMissions()
    {
        return Ok(dbContext.Missions.Include(x => x.Targets).Select(MissionViewModel.FromDbModel).ToList());
    }
}