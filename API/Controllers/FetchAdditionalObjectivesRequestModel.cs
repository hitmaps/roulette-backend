namespace RouletteBackend.Controllers;

public class FetchAdditionalObjectivesRequestModel
{
    public string MissionGame { get; set; } = null!;
    public string MissionLocation { get; set; } = null!;
    public string MissionSlug { get; set; } = null!;
    public string ObjectiveName { get; set; } = null!;
}