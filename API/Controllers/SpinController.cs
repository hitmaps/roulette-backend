using Microsoft.AspNetCore.Mvc;
using RouletteBackend.BusinessLogic;
using RouletteBackend.Controllers.ViewModels;
using RouletteBackend.ViewModels;

namespace RouletteBackend.Controllers;

public class SpinController : ApiControllerBase
{
    private readonly ISpinService _spinService;
    private readonly IShareSpinService _shareSpinService;

    public SpinController(ISpinService spinService, IShareSpinService shareSpinService)
    {
        _spinService = spinService;
        _shareSpinService = shareSpinService;
    }

    [HttpGet("spins")]
    public IActionResult GetSpinByPublicId(string publicId)
    {
        return Ok(_shareSpinService.GetSpin(publicId));
    }

    [HttpPost("spins")]
    public async Task<IActionResult> GenerateSpin(GenerateSpinRequestModel requestModel)
    {
        if (!requestModel.CriteriaFilters.IsModelValid())
        {
            return BadRequest(new
            {
                Message =
                    "Your spin criteria is invalid. At least one of specificMelee, specificFirearms, specificAccidents, or genericKills must be selected"
            });
        }
        
        var generatedSpin = await _spinService.GenerateSpinAsync(requestModel);
        return Ok(new SpinResultViewModel
        {
            Mission = MissionViewModel.FromDbModel(generatedSpin.Mission),
            TargetConditions = generatedSpin.TargetConditions.Select(x => new TargetConditionsViewModel
            {
                Target = TargetViewModel.FromDbModel(x.Target),
                KillMethod = KillMethodViewModel.FromGeneratedSpin(x.KillMethod),
                Disguise = DisguiseViewModel.FromDisguise(x.Disguise),
                Complications = x.Complications.Select(ComplicationViewModel.FromComplication).ToList()
            }).ToList(),
            AdditionalObjectives = generatedSpin.AdditionalObjectives.Select(x => new ObjectiveConditionsViewModel
            {
                Objective = ObjectiveViewModel.FromDbModel(x.Objective),
                CompletionMethod = CompleteMethodViewModel.FromDbModel(x.CompletionMethod),
                Disguise = DisguiseViewModel.FromDisguise(x.Disguise)
            }).ToList()
        });
    }

    [HttpGet("spins/kill-conditions")]
    public async Task<IActionResult> FetchKillConditions([FromQuery] FetchKillMethodsRequestModel requestModel)
    {
        var killMethods = await _spinService.FetchKillMethodsAsync(requestModel);
        
        return Ok(killMethods.SelectMany(innerKills => innerKills.KillMethods.Select(BasicKillMethodViewModel.FromBasicKillMethod)));
    }

    [HttpGet("spins/additional-objectives")]
    public IActionResult FetchAdditionalObjectives([FromQuery] FetchAdditionalObjectivesRequestModel requestModel)
    {
        var additionalObjectives = _spinService.FetchAdditionalObjectiveCompletionMethods(requestModel);

        return Ok(additionalObjectives.Select(CompleteMethodViewModel.FromDbModel).ToList());
    }

    [HttpPost("spins/share")]
    public IActionResult ShareSpin(SpinResultViewModel viewModel)
    {
        if (viewModel.TargetConditions.Any(x => x.Complications == null))
        {
            return BadRequest(new
            {
                Message = "Complications must not be null when sharing a spin. Replace them with empty arrays."
            });
        }
        
        var savedSpin = _shareSpinService.ShareSpin(viewModel);
        
        return Ok(new
        {
            PublicId = PublicIdHelper.FormatPublicId(savedSpin.PublicId)
        });
    }
}