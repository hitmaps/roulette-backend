using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using RouletteBackend.BusinessLogic.Exceptions;

namespace RouletteBackend.Controllers;

public class ExceptionFilter : IActionFilter, IOrderedFilter
{
    public int Order => int.MaxValue - 10;

    public void OnActionExecuting(ActionExecutingContext context)
    {
        
    }

    public void OnActionExecuted(ActionExecutedContext context)
    {
        if (context.Exception is not AbstractHttpResponseException exception)
        {
            return;
        }
        
        context.Result = new ObjectResult(exception.GetMessage())
        {
            StatusCode = exception.GetStatusCode()
        };

        context.ExceptionHandled = true;
    }
}