namespace RouletteBackend.ViewModels;

public class CriteriaFilterViewModel
{
    public bool SpecificDisguises { get; set; }
    public bool SpecificMelee { get; set; }
    public bool SpecificFirearms { get; set; }
    public bool SpecificAccidents { get; set; }
    public bool UniqueTargetKills { get; set; }
    public bool GenericKills { get; set; }
    public bool ImpossibleOrDifficultKills { get; set; }
    public bool AdditionalObjectives { get; set; }
    public bool AdditionalObjectiveDisguises { get; set; }
    public List<CriteriaComplicationsModel> PotentialComplications { get; set; } = new();

    public bool IsModelValid()
    {
        return ComplicationsValid() && (SpecificMelee || SpecificFirearms || SpecificAccidents || GenericKills);
    }

    private bool ComplicationsValid()
    {
        return PotentialComplications.All(x => x.IsValid());
    }
}