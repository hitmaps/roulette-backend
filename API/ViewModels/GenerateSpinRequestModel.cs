using RouletteBackend.Controllers.ViewModels;

namespace RouletteBackend.ViewModels;

public class GenerateSpinRequestModel
{
    public List<string> MissionPool { get; set; } = null!;
    public CriteriaFilterViewModel CriteriaFilters { get; set; } = null!;
    public List<TargetConditionsViewModel> LockedTargetConditions { get; set; } = new();
    public List<ObjectiveConditionsViewModel> LockedAdditionalObjectives { get; set; } = new();
}