namespace RouletteBackend.ViewModels;

public class CriteriaComplicationsModel
{
    public string ComplicationType { get; set; } = null!;
    public decimal OddsOfReceivingComplication { get; set; } = decimal.Zero;

    public bool IsValid()
    {
        return OddsOfReceivingComplication >= decimal.Zero;
    }
}