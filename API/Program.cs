using DataAccess.Data;
using DataAccess.Repositories;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using RouletteBackend.BusinessLogic;
using RouletteBackend.BusinessLogic.Discord;
using RouletteBackend.BusinessLogic.HitmapsDotCom;
using RouletteBackend.BusinessLogic.Redis;
using RouletteBackend.Controllers;
using RouletteBackend.Hubs;
using StackExchange.Redis;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddApiVersioning(options =>
{
    options.DefaultApiVersion = new ApiVersion(1, 0);
    options.ReportApiVersions = true;
    options.AssumeDefaultVersionWhenUnspecified = true;
    options.ApiVersionReader = new HeaderApiVersionReader("x-api-version");
});
builder.Services.AddDbContext<RouletteDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("Roulette")));
builder.Services.AddScoped<ISpinService, SpinService>();
builder.Services.AddScoped<IShareSpinService, ShareSpinService>();
builder.Services.AddScoped<IMatchupService, MatchupService>();
builder.Services.AddScoped<IHitmapsDotComService, HitmapsDotComService>();
builder.Services.AddSingleton<IHitmapsTournamentsService, HitmapsTournamentsService>();
builder.Services.AddSingleton<HttpClientWrapper>();
builder.Services.AddSingleton<IDiscordService, DiscordService>();
builder.Services.AddSingleton<IMatchHubService, MatchHubService>();
builder.Services.AddScoped<IMessageService, MessageService>();
builder.Services.AddScoped<CompletedObjectiveRepository>();
builder.Services.AddScoped<MatchupRepository>();
builder.Services.AddSingleton<RandomHandler>();
builder.Services.AddSingleton<IConnectionMultiplexer>(
    ConnectionMultiplexer.Connect(builder.Configuration.GetConnectionString("Redis")));
builder.Services.AddSingleton<IRedisClient, RedisClient>();
builder.Services.AddControllers(options =>
{
    options.Filters.Add<ExceptionFilter>();
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddCors(o => o.AddPolicy("CorsPolicy", x => 
    x.AllowAnyMethod()
        .AllowAnyHeader()
        .AllowCredentials()
        .SetIsOriginAllowed(_ => true)));
builder.Services.AddSignalR().AddJsonProtocol();

var app = builder.Build();

app.UseCors("CorsPolicy");

// SignalR
app.MapHub<MatchHub>("/ws/match-hub");

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// Apply EF migrations if in production, as we use command line otherwise
if (app.Environment.IsProduction())
{
    using var scope = app.Services.CreateScope();
    var db = scope.ServiceProvider.GetRequiredService<RouletteDbContext>();
    db.Database.Migrate();
    app.UseExceptionHandler("/error");
}

app.UseForwardedHeaders(new ForwardedHeadersOptions
{
    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
});

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();