using System.Text.Json;
using DataAccess.Data;
using DataAccess.Models.EF;
using Microsoft.EntityFrameworkCore;
using RouletteBackend.BusinessLogic.ApiResponses;
using RouletteBackend.BusinessLogic.KillMethods;
using RouletteBackend.BusinessLogic.Redis;
using RouletteBackend.Controllers;
using RouletteBackend.Controllers.ViewModels;
using RouletteBackend.ViewModels;

namespace RouletteBackend.BusinessLogic;

public class SpinService : ISpinService
{
    private readonly RouletteDbContext dbContext;
    private readonly RandomHandler randomHandler;
    private readonly IRedisClient redisClient;

    public SpinService(RouletteDbContext dbContext, RandomHandler randomHandler, IRedisClient redisClient)
    {
        this.dbContext = dbContext;
        this.randomHandler = randomHandler;
        this.redisClient = redisClient;
    }

    public async Task<List<KillCategory>> FetchKillMethodsAsync(FetchKillMethodsRequestModel requestModel)
    {
        // Yes this looks duplicated, because it is. Can't think of a nice way to split
        // this up without calling DB logic twice.
        var hitmapsMissionInfo =
            await GetWeaponAndDisguiseInfoFromHitmaps(requestModel.MissionGame, 
                requestModel.MissionLocation, 
                requestModel.MissionSlug,
                requestModel.MissionVariant);
        //@formatter:off
        var missionInfo = dbContext.Missions
            .Include(x => x.Targets)
                .ThenInclude(x => x.UniqueKills)
            .Include(x => x.Targets)
                .ThenInclude(x => x.BlacklistedKills)
            .Include(x => x.Targets)
                .ThenInclude(x => x.KillOverrides)
            .FirstOrDefault(x => x.Slug == requestModel.MissionSlug && x.VariantSlug == hitmapsMissionInfo.Variant);
        //@formatter:on
        if (missionInfo == null)
        {
            throw new MissingMissionException(requestModel.MissionSlug);
        }

        var target = missionInfo.Targets.First(x => x.Name == requestModel.TargetName);
        var killOverrides = target.KillOverrides;
        var killMethods = killOverrides.Any() ? 
            ConvertKillOverrides(killOverrides) : 
            BuildPossibleKills(hitmapsMissionInfo, requestModel);
        
        AddUniqueKills(target, requestModel, killMethods);

        return killMethods;
    }

    public List<KillCategory> ConvertKillOverrides(List<KillOverride> killOverrides)
    {
        //-- Include firearms + the overrides
        return new List<KillCategory>
        {
            new KillCategory
            {
                KillType = KillType.Generic,
                KillMethods = killOverrides.Select(x => new BasicKillMethod
                {
                    Chosen = false,
                    LargeWeapon = false,
                    Name = x.Name,
                    Remote = false,
                    TileUrl = x.TileUrl
                }).ToList()
            },
            new KillCategory
            {
                KillType = KillType.SpecificFirearm,
                KillMethods = dbContext.Kills.Where(x => x.Type == KillType.SpecificFirearm &&
                                                      x.Name != "Explosive (Weapon)" &&
                                                      x.Name != "SMG Elimination" &&
                                                      x.Name != "Pistol Elimination" &&
                                                      x.Name != "Weapon Elimination")
                .Select(x => new BasicKillMethod
                {
                    Name = x.Name,
                    TileUrl = x.TileUrl,
                    LargeWeapon = x.LargeWeapon,
                    Variants = x.Variants.Select(y => new Variant
                    {
                        Name = y.Name,
                        LargeWeapon = y.LargeWeapon,
                        ImageOverride = y.ImageOverride,
                        Remote = y.Remote
                    }).ToList()
                }).ToList()
            }
        };
    }

    public List<AdditionalObjectiveOption> FetchAdditionalObjectiveCompletionMethods(FetchAdditionalObjectivesRequestModel requestModel)
    {
        //@formatter:off
        var missionInfo = dbContext.Missions
            .Include(x => x.AdditionalObjectives)
                .ThenInclude(x => x.CompletionOptions)
            .First(x => x.Slug == requestModel.MissionSlug);
        //@formatter:on

        return missionInfo.AdditionalObjectives.First(x => x.Name == requestModel.ObjectiveName).CompletionOptions;
    }

    public async Task<GeneratedSpin> GenerateSpinAsync(GenerateSpinRequestModel requestModel)
    {
        var selectedMission = randomHandler.GetRandomElementFromList(requestModel.MissionPool);
        var gameInformation = selectedMission.Split('|');
        var variant = gameInformation.Length > 3 ? gameInformation[3] : null;
        var hitmapsMissionInfo =
            await GetWeaponAndDisguiseInfoFromHitmaps(gameInformation[0], gameInformation[1], gameInformation[2], variant);
        //@formatter:off
        var missionInfo = dbContext.Missions
            .Include(x => x.AdditionalObjectives)
                .ThenInclude(x => x.CompletionOptions)
            .Include(x => x.Targets)
                .ThenInclude(x => x.UniqueKills)
            .Include(x => x.Targets)
                .ThenInclude(x => x.BlacklistedKills)
            .Include(x => x.Targets)
                .ThenInclude(x => x.KillOverrides)
            .AsSplitQuery()
            .FirstOrDefault(x => x.Slug == gameInformation[2] && x.VariantSlug == hitmapsMissionInfo.Variant);
        //@formatter:on
        if (missionInfo == null)
        {
            throw new MissingMissionException(gameInformation[2]);
        }

        var possibleKills = BuildPossibleKills(hitmapsMissionInfo, requestModel.CriteriaFilters);

        var targetConditions = new List<TargetSpinResult>();
        var additionalObjectives = new List<ObjectiveSpinResult>();
        var usedLargeWeapon = BlockLockedConditions(requestModel.LockedTargetConditions, possibleKills, hitmapsMissionInfo, missionInfo);
        
        #region Targets
        foreach (var target in missionInfo.Targets)
        {
            var lockedConditions = requestModel.LockedTargetConditions.FirstOrDefault(x => x.Target.Name == target.Name);
            #region Disguise
            var selectedDisguise = Disguise.AnyDisguise;
            if (lockedConditions?.Disguise != null)
            {
                selectedDisguise = hitmapsMissionInfo.Disguises.First(x => x.Name == lockedConditions.Disguise.Name);
            }
            else if (requestModel.CriteriaFilters.SpecificDisguises)
            {
                    selectedDisguise = randomHandler.GetRandomElementFromList(hitmapsMissionInfo.Disguises
                        .Where(x => !x.Chosen && 
                                    !target.BlacklistedKills.Any(y => y.Disguise == x.Name && y.KillName == null)).ToList());
                    selectedDisguise.Chosen = true;
            }
            #endregion
            #region KillMethod
            BasicKillMethod selectedKill;
            if (lockedConditions?.KillMethod != null)
            {
                selectedKill = possibleKills.SelectMany(x => x.KillMethods)
                    .First(x => x.Name == lockedConditions.KillMethod.Name);
            }
            else
            {
                var availableKills = target.KillOverrides.Any() ? 
                    ConvertKillOverrides(target.KillOverrides) : 
                    GetAvailableKills(possibleKills, target, usedLargeWeapon, selectedDisguise, requestModel.CriteriaFilters);
                var killCategory = randomHandler.GetRandomElementFromList(availableKills);
                selectedKill = randomHandler.GetRandomElementFromList(killCategory.KillMethods);
                selectedKill.Chosen = true;

                // TODO Move this out of here
                var rngPercentage = selectedKill.Name == "Explosive (Weapon)" ? 100 : 50;
                if (selectedKill.Variants.Any() && target.AllowsWeaponVariants && randomHandler.IsRngInYourFavor(rngPercentage))
                {
                    var disguiseIsRemoteTrap = target.BlacklistedKills.Any(y =>
                        y.Disguise == selectedDisguise.Name && y.KillName == "REMOTE-ONLY");
                    var availableVariants = selectedKill.Variants.Where(x => 
                        (!disguiseIsRemoteTrap || x.Remote) && 
                        (!usedLargeWeapon || !x.LargeWeapon)).ToList();

                    if (availableVariants.Any())
                    {
                        var selectedVariant = randomHandler.GetRandomElementFromList(availableVariants);
                        if (selectedVariant.LargeWeapon)
                        {
                            usedLargeWeapon = true;
                        }

                        selectedVariant.Chosen = true;
                    }
                }

                if (selectedKill.LargeWeapon)
                {
                    usedLargeWeapon = true;
                }
            }
            #endregion

            // ReSharper disable once MergeIntoPattern
            var selectedKillNameWithVariant = $"{selectedKill.Variants.FirstOrDefault(x => x.Chosen)?.Name} {selectedKill.Name}";
            var complications = lockedConditions?.KillMethod != null && lockedConditions.Complications != null ?
                ConvertLockedComplications(lockedConditions.Complications) :
                requestModel.CriteriaFilters.PotentialComplications
                    .Where(x => !target.BlacklistedKills.Any(blacklistedKill => blacklistedKill.Complication == x.ComplicationType && 
                        (blacklistedKill.KillName == null || blacklistedKill.KillName == selectedKill.Name || blacklistedKill.KillName == selectedKillNameWithVariant) && 
                        (blacklistedKill.Disguise == null || blacklistedKill.Disguise == selectedDisguise.Name))).ToList();
            
            targetConditions.Add(new TargetSpinResult
            {
                Target = target,
                KillMethod = selectedKill,
                Disguise = selectedDisguise,
                Complications = BuildComplications(complications, selectedKill, selectedDisguise)
            });
        }
        #endregion
        #region Additional Objectives
        if (requestModel.CriteriaFilters.AdditionalObjectives)
        {
            foreach (var additionalObjective in missionInfo.AdditionalObjectives)
            {
                #region Disguise
                var lockedConditions = requestModel.LockedAdditionalObjectives
                    .FirstOrDefault(x => x.Objective.Name == additionalObjective.Name);
                var selectedDisguise = Disguise.AnyDisguise;
                if (lockedConditions?.Disguise != null)
                {
                    selectedDisguise =
                        hitmapsMissionInfo.Disguises.First(x => x.Name == lockedConditions.Disguise.Name);
                }
                else if (requestModel.CriteriaFilters.AdditionalObjectiveDisguises)
                {
                    selectedDisguise =
                        randomHandler.GetRandomElementFromList(hitmapsMissionInfo.Disguises.Where(x => !x.Chosen)
                            .ToList());
                    selectedDisguise.Chosen = true;
                }
                #endregion
                #region Completion Method
                AdditionalObjectiveOption selectedCompletionMethod;
                if (lockedConditions?.CompletionMethod != null)
                {
                    selectedCompletionMethod =
                        additionalObjective.CompletionOptions.First(x =>
                            x.Name == lockedConditions.CompletionMethod.Name);
                }
                else
                {
                    selectedCompletionMethod =
                        randomHandler.GetRandomElementFromList(additionalObjective.CompletionOptions);
                }
                #endregion
                
                additionalObjectives.Add(new ObjectiveSpinResult
                {
                    Objective = additionalObjective,
                    CompletionMethod = selectedCompletionMethod,
                    Disguise = selectedDisguise
                });
            }
        }
        #endregion

        return new GeneratedSpin
        {
            Mission = missionInfo,
            TargetConditions = targetConditions,
            AdditionalObjectives = additionalObjectives
        };
    }

    private async Task<HitmapsMissionInfo> GetWeaponAndDisguiseInfoFromHitmaps(string game, string location, string mission, string? variantSlug)
    {
        var redisKey = KeyBuilder.BuildKey(game, location, mission, variantSlug ?? string.Empty);
        #region Redis Lookup
        var cachedValue = redisClient.Get<HitmapsMissionInfo>(redisKey);
        if (cachedValue != null)
        {
            return cachedValue;
        }
        #endregion
        
        using var httpClient = new HttpClient();
        #region Get Default Mission Difficulty

        var response =
            await httpClient.GetAsync(
                $"https://api.hitmaps.com/api/games/{game}/locations/{location}/missions/{mission}");
        if (!response.IsSuccessStatusCode)
        {
            throw new Exception("Failed to fetch mission metadata from HITMAPS API");
        }

        var missionInfo = await JsonSerializer.DeserializeAsync<HitmapsMissionResponse>(
            await response.Content.ReadAsStreamAsync(), new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });
        HitmapsMissionNameCache.MissionSlugToName.TryAdd(mission, missionInfo!.Name);
        missionInfo.Variants.ForEach(variant => HitmapsMissionNameCache.MissionSlugToName.TryAdd($"{mission}|{variant.Slug}", variant.Name));
        var missionVariant = variantSlug == null ? 
            missionInfo.Variants.First(x => x.Default) : 
            missionInfo.Variants.First(x => x.Slug == variantSlug);
        
        #endregion
        #region Lethal Melee Items
        response = await httpClient.GetAsync($"https://api.hitmaps.com/api/games/{game}/locations/{location}/missions/{mission}/nodes");
        if (!response.IsSuccessStatusCode)
        {
            throw new Exception("Failed to fetch items from HITMAPS API");
        }

        var nodes = await JsonSerializer.DeserializeAsync<HitmapsNodeResponse>(
            await response.Content.ReadAsStreamAsync(), new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });
        
        // Only select lethal melees that aren't fiber wire reskins
        var fiberWireReskins = new List<string> { "Fiber Wire", "Earphones", "Measuring Tape", "Stethoscope", "Aztec Necklace", "Fishing Line" };
        var lethalMelees = nodes!.Nodes.Where(x => x.Group == "Lethal Melee" &&
                                                   x.Type == "Weapons and Tools" &&
                                                   x.Variants.Contains(missionVariant.Id) &&
                                                   !fiberWireReskins.Contains(x.Name))
            .DistinctBy(x => x.Name)
            .ToList();
        #endregion
        #region Disguises

        response =
            await httpClient.GetAsync(
                $"https://api.hitmaps.com/api/games/{game}/locations/{location}/missions/{mission}/disguises");
        if (!response.IsSuccessStatusCode)
        {
            throw new Exception("Failed to fetch items from HITMAPS API");
        }
        var disguises = await JsonSerializer.DeserializeAsync<HitmapsDisguisesResponse>(
            await response.Content.ReadAsStreamAsync(), new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });
        var foundASuit = false;
        var validDisguises = new List<Disguise>();
        foreach (var disguise in disguises!.Disguises.Where(disguise => !disguise.Suit || !foundASuit))
        {
            if (disguise.Suit)
            {
                foundASuit = true;
            }
            
            validDisguises.Add(new Disguise
            {
                Name = disguise.Suit ? "Suit" : disguise.Name,
                TileUrl = disguise.Image
            });
        }

        // TODO Move to database
        if (mission == "finish-line" && (variantSlug == "the-undying" || variantSlug == "the-undying-returns"))
        {
            validDisguises.Add(new Disguise
            {
                Name = "Timothy Yu",
                TileUrl = "https://media.hitmaps.com/img/hitman3/unlockables/outfit_73ed92b4-81f8-4a09-9249-aac0a2b7eaa6_0.jpg"
            });
        }
        else if (mission == "ark-society" && variantSlug == "the-disruptor")
        {
            validDisguises.Add(new Disguise
            {
                Name = "Tim Quinn",
                TileUrl = "#"
            });
        }
        #endregion

        var retVal = new HitmapsMissionInfo
        {
            Disguises = validDisguises,
            LethalMelee = lethalMelees,
            Variant = missionVariant.Slug
        };
        redisClient.Set(redisKey, retVal);
        return retVal;
    }
    
    private List<KillCategory> BuildPossibleKills(HitmapsMissionInfo hitmapsMissionInfo,
        CriteriaFilterViewModel criteriaFilters)
    {
        var possibleKills = new List<KillCategory>();

        if (criteriaFilters.GenericKills)
        {
            possibleKills.Add(new KillCategory
            {
                KillType = KillType.Generic,
                KillMethods = dbContext.Kills.Where(x => x.Type == KillType.Generic).Select(x => new BasicKillMethod
                {
                    Name = x.Name,
                    TileUrl = x.TileUrl,
                    LargeWeapon = x.LargeWeapon,
                    Remote = x.Remote,
                    KillType = x.Type
                }).ToList()
            });
        }
        if (criteriaFilters.SpecificFirearms)
        {
            possibleKills.Add(new KillCategory
            {
                KillType = KillType.SpecificFirearm,
                KillMethods = dbContext.Kills.Where(x => x.Type == KillType.SpecificFirearm).Select(x => new BasicKillMethod
                {
                    Name = x.Name,
                    TileUrl = x.TileUrl,
                    LargeWeapon = x.LargeWeapon,
                    Variants = x.Variants.Select(y => new Variant
                    {
                        Name = y.Name,
                        LargeWeapon = y.LargeWeapon,
                        ImageOverride = y.ImageOverride,
                        Remote = y.Remote
                    }).ToList(),
                    Remote = x.Remote,
                    KillType = x.Type
                }).ToList()
            });
        }
        if (criteriaFilters.SpecificAccidents)
        {
            possibleKills.Add(new KillCategory
            {
                KillType = KillType.SpecificAccidentOrPoison,
                KillMethods = dbContext.Kills.Where(x => x.Type == KillType.SpecificAccidentOrPoison).Select(x => new BasicKillMethod
                {
                    Name = x.Name,
                    TileUrl = x.TileUrl,
                    Remote = x.Remote,
                    KillType = x.Type,
                    Variants = x.Variants.Select(y => new Variant
                    {
                        Name = y.Name,
                        LargeWeapon = y.LargeWeapon,
                        ImageOverride = y.ImageOverride,
                        Remote = y.Remote
                    }).ToList()
                }).ToList()
            });
        }
        // ReSharper disable once InvertIf
        if (criteriaFilters.SpecificMelee)
        {
            var allMelee = hitmapsMissionInfo.LethalMelee.Select(x => new MeleeKillMethod
            {
                Name = x.Name,
                TileUrl = x.Image,
                Remote = false,
                KillType = KillType.SpecificMelee
            }).ToList();
            dbContext.Kills.Where(x => x.Type == KillType.SpecificMelee).ToList().ForEach(x => allMelee.Add(new MeleeKillMethod
            {
                Name = x.Name,
                TileUrl = x.TileUrl,
                KillType = x.Type,
                Remote = false
            }));
            possibleKills.Add(new KillCategory
            {
                KillType = KillType.SpecificMelee,
                KillMethods = new List<BasicKillMethod>(allMelee)
            });
        }
        

        return possibleKills;
    }

    private bool BlockLockedConditions(List<TargetConditionsViewModel> lockedConditions,
        List<KillCategory> possibleKills,
        HitmapsMissionInfo hitmapsMissionInfo, 
        Mission missionInfo)
    {
        var usedLargeWeapon = false;
        // 1. Block disguises
        lockedConditions.ForEach(x =>
        {
            if (missionInfo.Targets.All(y => y.Name != x.Target.Name))
            {
                return;
            }
            
            if (x.Disguise != null && x.Disguise.Name != "Any Disguise")
            {
                hitmapsMissionInfo.Disguises.First(y => y.Name == x.Disguise.Name).Chosen = true;
            }

            // ReSharper disable once InvertIf
            if (x.KillMethod != null)
            {
                var killMethod = possibleKills.SelectMany(y => y.KillMethods)
                    .First(y => y.Name == x.KillMethod.Name);
                killMethod.Chosen = true;
                if (killMethod.LargeWeapon)
                {
                    usedLargeWeapon = true;
                }

                // ReSharper disable once InvertIf
                if (x.KillMethod.SelectedVariant != null)
                {
                    var variant = killMethod.Variants.First(y => y.Name == x.KillMethod.SelectedVariant);
                    variant.Chosen = true;
                    if (variant.LargeWeapon)
                    {
                        usedLargeWeapon = true;
                    }
                }
            }
        });

        return usedLargeWeapon;
    }

    private List<KillCategory> GetAvailableKills(List<KillCategory> possibleKills, 
        Target target, 
        bool usedLargeWeapon, 
        Disguise selectedDisguise,
        CriteriaFilterViewModel criteriaFilters)
    {
        var actuallyPossibleKills = new List<KillCategory>();
        // I could remove this first iteration, but then I'd have to handle removing unique kills after each target... so meh ;)
        // ReSharper disable once ForeachCanBeConvertedToQueryUsingAnotherGetEnumerator
        foreach (var killCategory in possibleKills)
        {
            var possibleKillsInCategory = GetAvailableKillsForCategory(killCategory,
                target,
                usedLargeWeapon,
                selectedDisguise,
                criteriaFilters);

            actuallyPossibleKills.Add(possibleKillsInCategory);
        }

        AddUniqueKills(target, criteriaFilters, actuallyPossibleKills);
        
        // ReSharper disable once ForeachCanBeConvertedToQueryUsingAnotherGetEnumerator
        foreach (var killCategory in actuallyPossibleKills)
        {
            var possibleKillsInCategory = GetAvailableKillsForCategory(killCategory,
                target,
                usedLargeWeapon,
                selectedDisguise,
                criteriaFilters);

            actuallyPossibleKills.First(x => x.KillType == killCategory.KillType).KillMethods = possibleKillsInCategory.KillMethods;
        }

        return actuallyPossibleKills;
    }

    private static KillCategory GetAvailableKillsForCategory(KillCategory potentialKillMethods, 
        Target target, 
        bool usedLargeWeapon, 
        Disguise selectedDisguise,
        CriteriaFilterViewModel criteriaFilters)
    {
        return new KillCategory
        {
            KillType = potentialKillMethods.KillType,
            KillMethods = potentialKillMethods.KillMethods.Where(x =>
            {
                var killIsNotBlacklisted = !target.BlacklistedKills.Any(y => 
                    y.KillName == x.Name &&
                    (y.Disguise == null || y.Disguise == selectedDisguise.Name));
                var disguiseIsNotRemoteTrapOrKillIsRemote = !target.BlacklistedKills.Any(y =>
                    y.Disguise == selectedDisguise.Name && y.KillName == "REMOTE-ONLY" && !x.Remote);

                // ReSharper disable once InvertIf
                if (criteriaFilters.ImpossibleOrDifficultKills)
                {
                    killIsNotBlacklisted = true;
                    disguiseIsNotRemoteTrapOrKillIsRemote = true;
                }
                    
                return !x.Chosen && // Not already chosen
                       (!usedLargeWeapon ||
                        !x.LargeWeapon) && // Isn't a large weapon, or we haven't picked a large weapon yet
                       killIsNotBlacklisted &&
                       disguiseIsNotRemoteTrapOrKillIsRemote;
            }).ToList()
        };
    }

    private static void AddUniqueKills(Target target, CriteriaFilterViewModel criteriaFilters, List<KillCategory> killCategories)
    {
        if (!target.UniqueKills.Any() || !criteriaFilters.UniqueTargetKills)
        {
            return;
        }
        
        foreach (var uniqueKill in target.UniqueKills)
        {
            killCategories.FirstOrDefault(x => x.KillType == uniqueKill.KillType)?
                .KillMethods
                .Add(new BasicKillMethod
                {
                    LargeWeapon = false,
                    Name = uniqueKill.Name,
                    Remote = uniqueKill.Remote,
                    TileUrl = uniqueKill.TileUrl
                });
        }
    }

    private static List<CriteriaComplicationsModel> ConvertLockedComplications(IEnumerable<ComplicationViewModel> lockedConditionsComplications)
    {
        var complicationsAsList = lockedConditionsComplications.ToList();
        
        if (!complicationsAsList.Any())
        {
            return new List<CriteriaComplicationsModel>();
        }
        
        return complicationsAsList.Select(x => new CriteriaComplicationsModel
        {
            ComplicationType = x.Name,
            OddsOfReceivingComplication = 1
        }).ToList();
    }

    private List<Complication> BuildComplications(List<CriteriaComplicationsModel> potentialComplications, 
        BasicKillMethod selectedKill, 
        Disguise selectedDisguise)
    {
        var allComplications = dbContext.Complications
            .Include(x => x.SupportedKillTypes)
            .ToList();
        var builtComplications = new List<Complication>();
        // ReSharper disable once ForeachCanBeConvertedToQueryUsingAnotherGetEnumerator
        foreach (var potentialComplication in potentialComplications)
        {
            var percentChance = decimal.ToInt32(potentialComplication.OddsOfReceivingComplication * 100);
            if (!randomHandler.IsRngInYourFavor(percentChance))
            {
                continue;
            }
            
            var dbComplication = allComplications.First(x => x.Name == potentialComplication.ComplicationType);
            
            
            if (dbComplication.SupportedKillTypes.Select(x => x.KillType).Contains(selectedKill.KillType))
            {
                builtComplications.Add(new Complication
                {
                    Name = dbComplication.Name,
                    Description = dbComplication.Description,
                    TileUrl = dbComplication.TileUrl
                });
            }
        }

        return builtComplications;
    }
}