using System.Net.Http.Headers;
using RouletteBackend.BusinessLogic.Discord;
using RouletteBackend.BusinessLogic.Exceptions;

namespace RouletteBackend.BusinessLogic;

public class HitmapsTournamentsService : IHitmapsTournamentsService
{
    private readonly HttpClient httpClient;
    private readonly IConfiguration configuration;

    public HitmapsTournamentsService(IConfiguration configuration)
    {
        this.configuration = configuration;
        httpClient = new HttpClient();
        httpClient.DefaultRequestHeaders.Accept.Clear();
        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("accept/json"));
        httpClient.DefaultRequestHeaders.Add("User-Agent", "HITMAPS Roulette API");
    }

    public async Task IsUserAuthorizedForMatchAsync(int hitmapsTournamentsMatchId, DiscordUserContext? userContext)
    {
        if (userContext == null)
        {
            throw new AccessForbiddenException();
        }
        
        using var request = new HttpRequestMessage(HttpMethod.Get, 
            $"{configuration["TournamentsApiHost"]}/api/matches/{hitmapsTournamentsMatchId}/verify-authority");
        request.Headers.Add("x-discord-token", $"{userContext.TokenType} {userContext.TokenValue}");
        try
        {
            var response = await httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();
        }
        catch (HttpRequestException)
        {
            throw new AccessForbiddenException();
        }
    }
}