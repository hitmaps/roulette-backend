using RouletteBackend.BusinessLogic.ApiResponses;

namespace RouletteBackend.BusinessLogic;

public class HitmapsMissionInfo
{
    public List<Disguise> Disguises { get; set; } = new();
    public List<HitmapsNode> LethalMelee { get; set; } = new();
    public string Variant { get; set; } = null!;
}