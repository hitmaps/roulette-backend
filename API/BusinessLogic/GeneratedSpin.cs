using DataAccess.Models.EF;

namespace RouletteBackend.BusinessLogic;

public class GeneratedSpin
{
    public Mission Mission { get; set; } = null!;
    public List<TargetSpinResult> TargetConditions { get; set; } = new();
    public List<ObjectiveSpinResult> AdditionalObjectives { get; set; } = new();
}