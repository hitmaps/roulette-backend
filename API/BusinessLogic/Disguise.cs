namespace RouletteBackend.BusinessLogic;

public class Disguise
{
    public string Name { get; set; } = null!;
    public string TileUrl { get; set; } = null!;
    public bool Chosen { get; set; } = false;

    public static readonly Disguise AnyDisguise = new Disguise
    {
        Name = "Any Disguise",
        TileUrl = "https://media.hitmaps.com/img/hitman3/ui/tiles/any_disguise.jpg"
    };
}