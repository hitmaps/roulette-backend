using DataAccess.Models.EF;
using RouletteBackend.Controllers.ViewModels;

namespace RouletteBackend.BusinessLogic;

public interface IShareSpinService
{
    SpinResultViewModel GetSpin(string publicId);
    SavedSpin ShareSpin(SpinResultViewModel spinResultViewModel, int platformCode = 1);
}