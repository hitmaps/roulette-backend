namespace RouletteBackend.BusinessLogic.KillMethods;

public class Variant
{
    public string Name { get; set; } = null!;
    public bool LargeWeapon { get; set; }
    
    public bool Chosen { get; set; }
    public string? ImageOverride { get; set; }
    public bool Remote { get; set; }
}