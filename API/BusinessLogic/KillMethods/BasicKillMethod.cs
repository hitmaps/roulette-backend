using DataAccess.Models.EF;

namespace RouletteBackend.BusinessLogic.KillMethods;

public class BasicKillMethod
{
    public string Name { get; set; } = null!;
    public bool LargeWeapon { get; set; }
    public string TileUrl { get; set; } = null!;
    public List<Variant> Variants { get; set; } = new();
    public bool Remote { get; set; }
    public bool Chosen { get; set; }
    public KillType KillType { get; set; }
}