using DataAccess.Models.EF;

namespace RouletteBackend.BusinessLogic.KillMethods;

public class KillCategory
{
    public KillType KillType { get; set; }
    public List<BasicKillMethod> KillMethods { get; set; } = new();
}