namespace RouletteBackend.BusinessLogic.Extensions;

public static class DateTimeExtensions
{
    public static string ToApiDateFormat(this DateTime dateTime)
    {
        return dateTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
    }

    public static string? ToNullSafeApiDateFormat(this DateTime? dateTime)
    {
        return !dateTime.HasValue ? 
            null : 
            ToApiDateFormat(dateTime.Value);
    }
}