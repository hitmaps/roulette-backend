namespace RouletteBackend.BusinessLogic.HitmapsDotCom;

public class HitmapsLocationModel
{
    public string GameSlug { get; set; } = null!;
    public int Order { get; set; }
    public string Slug { get; set; } = null!;
    public string Destination { get; set; } = null!;
    public string BackgroundUrl { get; set; } = null!;
    public List<HitmapsMissionModel> Missions { get; set; } = new();
}

public class HitmapsMissionModel
{
    public int Id { set; get; }
    public string Slug { get; set; } = null!;
    public int Order { get; set; }
    public string Name { get; set; } = null!;
    public string TileUrl { get; set; } = null!;
    public string MissionType { get; set; } = null!;
    public List<HitmapsMissionVariant> Variants { get; set; } = new();
}

public class HitmapsMissionVariant
{
    public string Slug { get; set; } = null!;
    public string Icon { get; set; } = null!;
    public string Name { get; set; } = null!;
    public string? TileUrl { get; set; }
}