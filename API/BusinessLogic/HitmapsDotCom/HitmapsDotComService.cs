using System.Text.Json;
using RouletteBackend.BusinessLogic.Redis;

namespace RouletteBackend.BusinessLogic.HitmapsDotCom;

public class HitmapsDotComService : IHitmapsDotComService
{
    private readonly IRedisClient redisClient;
    private readonly HttpClientWrapper httpClientWrapper;
    private readonly IConfiguration configuration;

    public HitmapsDotComService(IRedisClient redisClient, HttpClientWrapper httpClientWrapper, IConfiguration configuration)
    {
        this.redisClient = redisClient;
        this.httpClientWrapper = httpClientWrapper;
        this.configuration = configuration;
    }

    public async Task<List<HitmapsLocationModel>> GetHitmapsLocationsAsync()
    {
        var cachedValues = redisClient.Get<List<HitmapsLocationModel>>(KeyBuilder.BuildKey("HitmapsLocations"));
        if (cachedValues != null)
        {
            return cachedValues;
        }
        
        var locations = new List<HitmapsLocationModel>();
        var supportedGames = configuration["SupportedGames"].Split(",");
        foreach (var supportedGame in supportedGames)
        {
            locations.AddRange(await GetHitmapsLocationByGameAsync(supportedGame));            
        }

        redisClient.Set(KeyBuilder.BuildKey("HitmapsLocations"), locations);
        return locations;
    }

    private async Task<List<HitmapsLocationModel>> GetHitmapsLocationByGameAsync(string game)
    {
        using var request = new HttpRequestMessage(HttpMethod.Get, $"https://api.hitmaps.com/api/games/{game}/locations");
        request.Headers.Add("x-api-version", "1.0");
        
        var response = await httpClientWrapper.GetClient().SendAsync(request);
        response.EnsureSuccessStatusCode();

        var locationModels = await JsonSerializer.DeserializeAsync<List<HitmapsLocationModel>>(await response.Content.ReadAsStreamAsync(), 
            new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
        locationModels!.ForEach(location => location.GameSlug = game);
        return locationModels;
    }
}