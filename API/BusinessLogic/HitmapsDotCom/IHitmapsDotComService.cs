namespace RouletteBackend.BusinessLogic.HitmapsDotCom;

public interface IHitmapsDotComService
{
    public Task<List<HitmapsLocationModel>> GetHitmapsLocationsAsync();
}