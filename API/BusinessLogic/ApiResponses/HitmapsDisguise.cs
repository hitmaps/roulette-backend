namespace RouletteBackend.BusinessLogic.ApiResponses;

public class HitmapsDisguise
{
    public string Name { get; set; } = null!;
    public bool Suit { get; set; }
    public string Image { get; set; } = null!;
}