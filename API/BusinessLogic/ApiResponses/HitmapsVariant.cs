namespace RouletteBackend.BusinessLogic.ApiResponses;

public class HitmapsVariant
{
    public int Id { get; set; }
    public string Slug { get; set; } = null!;
    public bool Default { get; set; }
    public string Name { get; set; } = null!;
}