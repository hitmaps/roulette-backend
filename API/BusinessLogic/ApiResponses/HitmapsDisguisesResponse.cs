namespace RouletteBackend.BusinessLogic.ApiResponses;

public class HitmapsDisguisesResponse
{
    public List<HitmapsDisguise> Disguises { get; set; } = new();
}