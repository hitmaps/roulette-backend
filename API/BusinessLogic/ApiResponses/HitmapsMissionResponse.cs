namespace RouletteBackend.BusinessLogic.ApiResponses;

public class HitmapsMissionResponse
{
    public string Name { get; set; } = null!;
    public List<HitmapsVariant> Variants { get; set; } = new();
}