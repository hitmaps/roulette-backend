namespace RouletteBackend.BusinessLogic.ApiResponses;

public class HitmapsNodeResponse
{
    public List<HitmapsNode> Nodes { get; set; } = new();
}