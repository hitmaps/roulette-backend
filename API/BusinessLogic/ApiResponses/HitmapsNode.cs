namespace RouletteBackend.BusinessLogic.ApiResponses;

public class HitmapsNode
{
    public string Type { get; set; } = null!;
    public string Group { get; set; } = null!;
    public string Name { get; set; } = null!;
    public string Image { get; set; } = null!;
    public List<int> Variants { get; set; } = new();
}