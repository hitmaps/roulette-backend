using DataAccess.Models.EF.Matchups;
using RouletteBackend.BusinessLogic.Discord;
using RouletteBackend.Controllers.ViewModels.Matchups;

namespace RouletteBackend.BusinessLogic;

public interface IMessageService
{
    Message? GetUnacknowledgedMessage(Guid matchupId, string playerPublicId);

    Task<Message> SendMessageAsync(Guid matchupId, SendMessageRequestModel requestModel,
        DiscordUserContext? userContext);

    Message ParticipantHandleMessage(Guid matchupId, string playerPublicId,
        ParticipantHandleMessageRequestModel requestModel);
}