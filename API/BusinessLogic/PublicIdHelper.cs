namespace RouletteBackend.BusinessLogic;

public static class PublicIdHelper
{
    public static string FormatPublicId(long publicId)
    {
        var stringPublicId = publicId.ToString();
        
        return $"{stringPublicId[0]}-{stringPublicId.Substring(1, 2)}-{stringPublicId.Substring(3, 7)}-{stringPublicId[10..]}";
    }
}