using RouletteBackend.BusinessLogic.Discord;

namespace RouletteBackend.BusinessLogic;

public interface IHitmapsTournamentsService
{
    Task IsUserAuthorizedForMatchAsync(int hitmapsTournamentsMatchId, DiscordUserContext? userContext);
}