using System.Text.Json;
using StackExchange.Redis;

namespace RouletteBackend.BusinessLogic.Redis;

public class RedisClient : IRedisClient
{
    private readonly IConnectionMultiplexer _redis;

    public RedisClient(IConnectionMultiplexer redis)
    {
        _redis = redis;
    }

    public T? Get<T>(string key)
    {
        var db = _redis.GetDatabase();
        
        var value = db.StringGet(key);
        return !value.HasValue ? default : 
            JsonSerializer.Deserialize<T>(value!);
    }

    public void Set<T>(string key, T value)
    {
        var db = _redis.GetDatabase();
        db.StringSet(key, JsonSerializer.Serialize(value));
    }
}