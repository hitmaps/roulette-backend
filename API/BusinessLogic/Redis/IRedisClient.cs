namespace RouletteBackend.BusinessLogic.Redis;

public interface IRedisClient
{
    T? Get<T>(string key);
    void Set<T>(string key, T value);
}

public static class KeyBuilder
{
    public static string BuildKey(params string[] parts)
    {
        return $"RouletteBackend::{string.Join(':', parts)}";
    }
}