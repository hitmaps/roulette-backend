using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using DataAccess.Data;
using DataAccess.Models.EF;
using Microsoft.EntityFrameworkCore;
using RouletteBackend.Controllers.ViewModels;

namespace RouletteBackend.BusinessLogic;

public class ShareSpinService : IShareSpinService
{
    private readonly RouletteDbContext _dbContext;
    private readonly RandomHandler _randomHandler;

    public ShareSpinService(RouletteDbContext dbContext, RandomHandler randomHandler)
    {
        _dbContext = dbContext;
        _randomHandler = randomHandler;
    }

    public SpinResultViewModel GetSpin(string publicId)
    {
        var spin = _dbContext.SavedSpins
            .Include(x => x.Mission)
                .ThenInclude(x => x.Targets)
            .Include(x => x.SavedConditions)
            .First(x => x.PublicId == long.Parse(publicId.Replace("-", "")));

        return SpinResultViewModel.FromSavedSpin(spin)!;
    }

    public SavedSpin ShareSpin(SpinResultViewModel spinResultViewModel, int platformCode = 1)
    {
        var mission = _dbContext.Missions
            .Include(x => x.Targets)
            .Include(x => x.AdditionalObjectives)
                .ThenInclude(x => x.CompletionOptions)
            .First(x => x.Slug == spinResultViewModel.Mission.Slug && x.VariantSlug == spinResultViewModel.Mission.VariantSlug);

        var existingSavedSpin = GetExistingSpin(spinResultViewModel);
        if (existingSavedSpin != null)
        {
            return existingSavedSpin;
        }

        var publicId = GeneratePublicId(platformCode, mission);
        var savedSpin = new SavedSpin
        {
            Mission = mission,
            PublicId = long.Parse(publicId.Replace("-", ""))
        };
        spinResultViewModel.TargetConditions.ForEach(x =>
        {
            if (x.KillMethod != null)
            {
                savedSpin.SavedConditions.Add(new SavedCondition
                {
                    EntityId = mission.Targets.First(y => y.Name == x.Target.Name).Id,
                    ConditionType = SavedConditionType.TargetKill.Value,
                    ConditionValue = JsonSerializer.Serialize(x.KillMethod)
                });
            }

            if (x.Disguise != null && x.Disguise!.Name != "Any Disguise")
            {
                savedSpin.SavedConditions.Add(new SavedCondition
                {
                    EntityId = mission.Targets.First(y => y.Name == x.Target.Name).Id,
                    ConditionType = SavedConditionType.TargetDisguise.Value,
                    ConditionValue = JsonSerializer.Serialize(x.Disguise)
                });
            }

            if (x.Complications!.Any())
            {
                x.Complications!.ForEach(complication => savedSpin.SavedConditions.Add(new SavedCondition
                {
                    EntityId = mission.Targets.First(y => y.Name == x.Target.Name).Id,
                    ConditionType = SavedConditionType.TargetComplication.Value,
                    ConditionValue = JsonSerializer.Serialize(complication)
                }));
            }
        });
        spinResultViewModel.AdditionalObjectives.ForEach(x =>
        {
            var objectiveId = mission.AdditionalObjectives.First(y => y.Name == x.Objective.Name).Id;
            savedSpin.SavedConditions.Add(new SavedCondition
            {
                EntityId = objectiveId,
                ConditionType = SavedConditionType.AdditionalObjectiveCompletionMethod.Value,
                ConditionValue = JsonSerializer.Serialize(x.CompletionMethod)
            });

            if (x.Disguise != null && x.Disguise!.Name != "Any Disguise")
            {
                savedSpin.SavedConditions.Add(new SavedCondition
                {
                    EntityId = objectiveId,
                    ConditionType = SavedConditionType.AdditionalObjectiveDisguise.Value,
                    ConditionValue = JsonSerializer.Serialize(x.Disguise)
                });
            }
        });
        savedSpin.SpinKey = GenerateSavedSpinLookupKey(spinResultViewModel);
        
        _dbContext.Update(savedSpin);
        _dbContext.SaveChanges();

        return savedSpin;
    }

    private SavedSpin? GetExistingSpin(SpinResultViewModel spinResultViewModel)
    {
        return _dbContext.SavedSpins
            .Include(x => x.SavedConditions)
            .FirstOrDefault(x => x.SpinKey == GenerateSavedSpinLookupKey(spinResultViewModel));
    }

    private string GeneratePublicId(int platformCode, Mission mission)
    {
        var usedNumbers = _dbContext.SavedSpins
            .Where(x => x.Mission == mission)
            .Select(x => x.PublicId).ToList();
        int randomNumber;
        do
        {
            randomNumber = _randomHandler.GetNext(0, 1_000_000_000);
        } while (usedNumbers.Contains(randomNumber));

        var paddedRandomNumber = randomNumber.ToString("D9");
        var missionPrefix = mission.PublicIdPrefix < 10 ? $"0{mission.PublicIdPrefix}" : $"{mission.PublicIdPrefix}";
        return $"{platformCode}-{missionPrefix}-{paddedRandomNumber[..7]}-{paddedRandomNumber[7..]}";
    }

    private static string GenerateSavedSpinLookupKey(SpinResultViewModel spinResultViewModel)
    {
        var jsonEntity = new Dictionary<string, object>();
        if (spinResultViewModel.TargetConditions.Any())
        {
            jsonEntity["TargetConditions"] = spinResultViewModel.TargetConditions
                .Select(targetCondition => targetCondition.Serialized())
                .ToList();
        }

        if (spinResultViewModel.AdditionalObjectives.Any())
        {
            jsonEntity["AdditionalObjectives"] = spinResultViewModel.AdditionalObjectives
                .Select(additionalObjective => additionalObjective.Serialized())
                .ToList();
        }

        using var hasher = SHA256.Create();
        var bytes = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(jsonEntity));
        return ToHex(hasher.ComputeHash(bytes));
    }
    
    private static string ToHex(byte[] bytes)
    {
        var result = new StringBuilder(bytes.Length * 2);
        foreach (var t in bytes)
        {
            result.Append(t.ToString("x2"));
        }

        return result.ToString();
    }
}