namespace RouletteBackend.BusinessLogic.Exceptions;

public class AccessForbiddenException : AbstractHttpResponseException
{
    public override string GetMessage()
    {
        return "Access is forbidden to this endpoint.";
    }

    public override int GetStatusCode()
    {
        return StatusCodes.Status403Forbidden;
    }
}