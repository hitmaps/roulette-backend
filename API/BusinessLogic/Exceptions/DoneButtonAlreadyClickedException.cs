namespace RouletteBackend.BusinessLogic.Exceptions;

public class DoneButtonAlreadyClickedException : Exception
{
    public DoneButtonAlreadyClickedException() : base("Done button already clicked; ignoring additional clicks")
    {
    }
}