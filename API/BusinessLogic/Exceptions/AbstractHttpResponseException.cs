namespace RouletteBackend.BusinessLogic.Exceptions;

public abstract class AbstractHttpResponseException : Exception
{
    public abstract string GetMessage();

    public abstract int GetStatusCode();
}