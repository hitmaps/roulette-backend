using DataAccess.Models.EF;

namespace RouletteBackend.BusinessLogic;

public class ObjectiveSpinResult
{
    public AdditionalObjective Objective { get; set; } = null!;
    public AdditionalObjectiveOption CompletionMethod { get; set; } = null!;
    public Disguise Disguise { get; set; } = null!;
}