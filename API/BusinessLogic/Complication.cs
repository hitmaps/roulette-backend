namespace RouletteBackend.BusinessLogic;

public class Complication
{
    public string Name { get; set; } = null!;
    public string Description { get; set; } = null!;
    public string TileUrl { get; set; } = null!;
}