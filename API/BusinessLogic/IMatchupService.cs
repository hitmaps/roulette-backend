using DataAccess.Models.EF.Matchups;
using RouletteBackend.BusinessLogic.Discord;
using RouletteBackend.Controllers.ViewModels;
using RouletteBackend.Controllers.ViewModels.Matchups;

namespace RouletteBackend.BusinessLogic;

public interface IMatchupService
{
    Matchup? GetMatchup(Guid matchId, string? playerId = null);

    Task<Matchup> CreateMatchupAsync(CreateMatchupRequestModel requestModel, DiscordUserContext? userContext);

    Task<Matchup> SendSpinAsync(Guid matchId, SendSpinRequestModel requestModel, DiscordUserContext? userContext);
    Task<Matchup> ClearSpinAsync(Guid matchupId, DiscordUserContext? userContext);
    Player MarkPlayerAsComplete(Guid matchupId, PlayerDoneRequestModel requestModel, DateTime completionTime);
    Task<Matchup> VerifySpinAsync(Guid matchupId, VerifySpinRequestModel requestModel, DiscordUserContext? userContext);
    Task<Matchup> DeleteMostRecentResult(Guid matchupId, int mapSelectionId, DiscordUserContext? userContext);
    Task<Matchup> UpdateForfeitAsync(Guid matchupId, string forfeitPlayerId, ForfeitRequestModel requestModel, DiscordUserContext? userContext);

    Task UpdateObjectivesAsync(Guid matchupId, UpdateObjectivesRequestModelCollection requestModel,
        DiscordUserContext? userContext);

    Task<Matchup> ClearObjectivesAsync(Guid matchupId, DiscordUserContext? userContext);

    List<Matchup> GetCompletedMatchupsForIds(List<Guid> matchIds);
}