using DataAccess.Models.EF;
using RouletteBackend.BusinessLogic.KillMethods;

namespace RouletteBackend.BusinessLogic;

public class TargetSpinResult
{
    public Target Target { get; set; } = null!;
    public BasicKillMethod KillMethod { get; set; } = null!;
    public Disguise Disguise { get; set; } = null!;
    public List<Complication> Complications { get; set; } = new();
}