
using Microsoft.AspNetCore.SignalR;
using RouletteBackend.Controllers.ViewModels;
using RouletteBackend.Controllers.ViewModels.Matchups;
using RouletteBackend.Hubs;

namespace RouletteBackend.BusinessLogic;

public class MatchHubService : IMatchHubService
{
    private readonly IHubContext<MatchHub, IMatchHub> matchHubContext;

    public MatchHubService(IHubContext<MatchHub, IMatchHub> matchHubContext)
    {
        this.matchHubContext = matchHubContext;
    }

    public async Task SendUpdatedMatchAsync(MatchupViewModel viewModel)
    {
        await matchHubContext.Clients.Group(string.Format(MatchHub.MatchParticipantsGroupFormat, viewModel.MatchupId.ToString())).MatchChange(viewModel);
    }

    public async Task SendPlayerDoneAsync(Guid matchupId, ParticipantViewModel viewModel)
    {
        await matchHubContext.Clients.Group(string.Format(MatchHub.MatchAdminGroupFormat, matchupId))
            .SpinComplete(viewModel);
    }

    public async Task SendMessageAcknowledgedAsync(Guid matchupId, MessageViewModel viewModel)
    {
        await matchHubContext.Clients.Group(string.Format(MatchHub.MatchAdminMessageAckGroupFormat, matchupId))
            .MessageAcknowledged(viewModel);
    }

    public async Task SendNewMessageAsync(Guid matchupId, MessageViewModel viewModel)
    {
        await matchHubContext.Clients.Group(string.Format(MatchHub.MatchParticipantMessageGroupFormat, matchupId,
                viewModel.PlayerPublicId))
            .NewMessage(viewModel);
    }

    public async Task SendObjectiveUpdateAsync(Guid matchupId, UpdateObjectivesRequestModelCollection updateObjectivesModel)
    {
        await matchHubContext.Clients.Group(string.Format(MatchHub.MatchAdminGroupFormat, matchupId))
            .ObjectiveUpdate(updateObjectivesModel);
    }
}