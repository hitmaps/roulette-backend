using RouletteBackend.Controllers.ViewModels;
using RouletteBackend.Controllers.ViewModels.Matchups;

namespace RouletteBackend.BusinessLogic;

public interface IMatchHubService
{
    Task SendUpdatedMatchAsync(MatchupViewModel viewModel);

    Task SendPlayerDoneAsync(Guid matchupId, ParticipantViewModel viewModel);

    Task SendMessageAcknowledgedAsync(Guid matchupId, MessageViewModel viewModel);

    Task SendNewMessageAsync(Guid matchupId, MessageViewModel viewModel);

    Task SendObjectiveUpdateAsync(Guid matchupId, UpdateObjectivesRequestModelCollection updateObjectivesModel);
}