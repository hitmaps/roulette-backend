using MersenneTwister;

namespace RouletteBackend.BusinessLogic;

public class RandomHandler
{
    private readonly Random random = Randoms.Create();

    public int GetNext(int lowerBound, int upperBound)
    {
        lock (random)
        {
            return random.Next(lowerBound, upperBound);
        }        
    }
    
    public int GetNext(int upperBound)
    {
        lock (random)
        {
            return random.Next(upperBound);
        }
    }

    public T GetRandomElementFromList<T>(List<T> list)
    {
        return list[GetNext(list.Count)];
    }

    public bool IsRngInYourFavor(int percentChance)
    {
        return GetNext(0, 100) < percentChance;
    }
}