using System.Security.Cryptography;
using DataAccess.Data;
using DataAccess.Models.EF.Matchups;
using DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using RouletteBackend.BusinessLogic.Discord;
using RouletteBackend.BusinessLogic.Exceptions;
using RouletteBackend.Controllers.ViewModels;
using RouletteBackend.Controllers.ViewModels.Matchups;

namespace RouletteBackend.BusinessLogic;

public class MatchupService : IMatchupService
{
    private readonly RouletteDbContext dbContext;
    private readonly IShareSpinService shareSpinService;
    private readonly IHitmapsTournamentsService hitmapsTournamentsService;
    private readonly CompletedObjectiveRepository completedObjectiveRepository;
    private readonly MatchupRepository matchupRepository;
    
    // TODO Should *probably* be an enum
    private const string VerifyResultReject = "Reject";
    private const string VerifyResultAccept = "Accept";
    private const string VerifyResultDraw = "Draw";

    public MatchupService(RouletteDbContext dbContext, 
        IShareSpinService shareSpinService, 
        IHitmapsTournamentsService hitmapsTournamentsService,
        CompletedObjectiveRepository completedObjectiveRepository,
        MatchupRepository matchupRepository)
    {
        this.dbContext = dbContext;
        this.shareSpinService = shareSpinService;
        this.hitmapsTournamentsService = hitmapsTournamentsService;
        this.completedObjectiveRepository = completedObjectiveRepository;
        this.matchupRepository = matchupRepository;
    }

    public Matchup? GetMatchup(Guid matchId, string? playerId = null)
    {
        var matchup = dbContext.Matchups
            .AsSplitQuery()
            .Include(x => x.MapSelections)
                .ThenInclude(y => y.Mission)
                .ThenInclude(y => y.Targets)
            .Include(x => x.Players)
            .Include(x => x.CurrentSpin)
                .ThenInclude(y => y!.Mission)
                .ThenInclude(y => y.Targets)
            .Include(x => x.CurrentSpin)
                .ThenInclude(y => y!.Mission)
                .ThenInclude(y => y.AdditionalObjectives)
                .ThenInclude(y => y.CompletionOptions)
            .Include(x => x.CurrentSpin)
                .ThenInclude(y => y!.SavedConditions)
            .Include(x => x.CompletedObjectives)
            .FirstOrDefault(x => x.MatchupId == matchId);
        if (matchup == null)
        {
            return null;
        }

        // ReSharper disable once InvertIf
        if (playerId != null)
        {
            var player = matchup.Players.First(x => x.PublicId == playerId);
            player.LastPing = DateTime.UtcNow;
            dbContext.Update(player);
            dbContext.SaveChanges();
        }

        return matchup;
    }

    private DataAccess.Models.Dapper.Matchup? GetBasicMatchup(Guid matchId)
    {
        return matchupRepository.GetForMatchupId(matchId);
    }

    public async Task<Matchup> CreateMatchupAsync(CreateMatchupRequestModel requestModel, DiscordUserContext? userContext)
    {
        if (requestModel.HitmapsTournamentsMatchId.HasValue && userContext != null)
        {
            await hitmapsTournamentsService.IsUserAuthorizedForMatchAsync(requestModel.HitmapsTournamentsMatchId.Value, userContext);
            
            //-- If we already have a match for this ID, return it
            var existingMatchup = dbContext.Matchups
                .Include(x => x.Players)
                .Include(x => x.MapSelections)
                    .ThenInclude(x => x.Mission)
                .FirstOrDefault(x => x.HitmapsTournamentsId == requestModel.HitmapsTournamentsMatchId);
            if (existingMatchup != null)
            {
                return existingMatchup;
            }
        }

        var matchup = new Matchup
        {
            HitmapsTournamentsId = requestModel.HitmapsTournamentsMatchId,
            MatchupId = Guid.NewGuid(),
            Players = requestModel.Players.Select(x => new Player
            {
                LastPing = DateTime.UnixEpoch,
                Name = x.Name,
                AvatarUrl = x.AvatarUrl,
                PublicId = GenerateRandomHexString()
            }).ToList(),
            MapSelections = requestModel.MapSelections
                .Select(mapSelection => new MapSelection
                {
                    Mission = dbContext.Missions.First(x => x.Slug == mapSelection.HitmapsSlug),
                    ChosenByName = mapSelection.ChosenByName
                })
                .ToList(),
            TimerPaused = false,
            TimerFingerprint = Guid.NewGuid(),
            OverlayTheme = requestModel.OverlayTheme,
            PointsForVictory = requestModel.PointsForVictory
        };

        dbContext.Update(matchup);
        await dbContext.SaveChangesAsync();

        return matchup;
    }

    private static string GenerateRandomHexString()
    {
        var value = RandomNumberGenerator.GetBytes(8);

        return string.Join("", value.Select(x => x.ToString("x2")));
    }

    public async Task<Matchup> SendSpinAsync(Guid matchId, SendSpinRequestModel requestModel, DiscordUserContext? userContext)
    {
        var matchup = dbContext.Matchups
            .Include(x => x.Players)
            .Include(x => x.CurrentSpin)
            .Include(x => x.CompletedObjectives)
            .Include(x => x.MapSelections)
            .First(x => x.MatchupId == matchId);

        if (matchup.HitmapsTournamentsId.HasValue)
        {
            await hitmapsTournamentsService.IsUserAuthorizedForMatchAsync(matchup.HitmapsTournamentsId.Value, userContext);
        }
        
        //-- Clear out old data
        matchup.Players.ForEach(player => player.CompleteTime = null);
        matchup.TimerPausedRemainingTimeInSeconds = null;
        matchup.TimerPaused = false;
        
        //-- Set new data
        matchup.CurrentSpin = shareSpinService.ShareSpin(requestModel.Spin);
        matchup.TimerStartTime = requestModel.StartTime < DateTime.UtcNow ? 
            DateTime.UtcNow : 
            requestModel.StartTime;
        matchup.TimerEndTime = SetEndTime(matchup.TimerStartTime, requestModel.SpinDuration);
        matchup.TimerFingerprint = Guid.NewGuid();
        matchup.CompletedObjectives.Clear();
        
        var currentMap = GetCurrentMap(matchup);
        if (matchup.HitmapsTournamentsId.HasValue && currentMap != null)
        {
            currentMap.SavedSpin = matchup.CurrentSpin;
            currentMap.MapStartedAt = matchup.TimerStartTime;
            dbContext.Update(currentMap);
        }

        dbContext.Update(matchup);
        await dbContext.SaveChangesAsync();

        return matchup;
    }

    private static MapSelection? GetCurrentMap(Matchup matchup)
    {
        return matchup.MapSelections
            .Where(x => !x.Complete)
            // ReSharper disable once SimplifyLinqExpressionUseMinByAndMaxBy
            .OrderBy(x => x.Id)
            .FirstOrDefault();
    }

    public async Task<Matchup> ClearSpinAsync(Guid matchupId, DiscordUserContext? userContext)
    {
        var matchup = dbContext.Matchups
            .Include(x => x.Players)
            .Include(x => x.CurrentSpin)
            .Include(x => x.CompletedObjectives)
            .First(x => x.MatchupId == matchupId);

        if (matchup.HitmapsTournamentsId.HasValue)
        {
            await hitmapsTournamentsService.IsUserAuthorizedForMatchAsync(matchup.HitmapsTournamentsId.Value, userContext);
        }
        
        matchup.Players.ForEach(player => player.CompleteTime = null);
        matchup.TimerPausedRemainingTimeInSeconds = null;
        matchup.TimerPaused = false;
        matchup.CurrentSpin = null;
        matchup.TimerStartTime = null;
        matchup.TimerEndTime = null;
        matchup.TimerFingerprint = Guid.NewGuid();
        matchup.CompletedObjectives.Clear();

        var currentMap = GetCurrentMap(matchup);
        if (matchup.HitmapsTournamentsId.HasValue && currentMap != null)
        {
            currentMap.MapStartedAt = null;
            currentMap.SavedSpin = null;
            dbContext.Update(currentMap);
        }

        dbContext.Update(matchup);
        await dbContext.SaveChangesAsync();

        return matchup;
    }

    private static DateTime? SetEndTime(DateTime? startTime, int? duration)
    {
        if (!duration.HasValue || !startTime.HasValue)
        {
            return null;
        }

        //-- Add 1 second to account for network latency
        return startTime.Value.AddMinutes(duration.Value).AddSeconds(1);
    }

    public Player MarkPlayerAsComplete(Guid matchupId, PlayerDoneRequestModel requestModel, DateTime completionTime)
    {
        var matchup = dbContext.Matchups
            .Include(x => x.Players)
            .Include(x => x.CurrentSpin)
            .First(x => x.MatchupId == matchupId);

        //-- Prevent desyncs
        if (matchup.TimerFingerprint != requestModel.TimerFingerprint)
        {
            throw new ConflictException<Guid>(matchup.TimerFingerprint, requestModel.TimerFingerprint);
        }

        var player = matchup.Players.First(x => x.PublicId == requestModel.PlayerPublicId);
        if (player.CompleteTime.HasValue)
        {
            throw new DoneButtonAlreadyClickedException();
        }

        player.CompleteTime = completionTime;

        dbContext.Update(player);
        dbContext.SaveChanges();

        return player;
    }

    public async Task<Matchup> VerifySpinAsync(Guid matchupId, VerifySpinRequestModel requestModel, DiscordUserContext? userContext)
    {
        if (!new List<string> { VerifyResultReject, VerifyResultAccept, VerifyResultDraw }.Contains(requestModel.VerifyResult))
        {
            throw new Exception("VerifyResult invalid");
        }
        
        var matchup = GetMatchup(matchupId)!;

        if (matchup.HitmapsTournamentsId.HasValue)
        {
            await hitmapsTournamentsService.IsUserAuthorizedForMatchAsync(matchup.HitmapsTournamentsId.Value, userContext);
        }

        if (requestModel.VerifyResult == VerifyResultReject)
        {
            var player = matchup.Players.First(x => x.PublicId == requestModel.PlayerPublicId);
            player.CompleteTime = null;
        }
        else
        {
            var mapSelection = matchup.MapSelections.First(x => x.Id == requestModel.MapSelectionId);
            mapSelection.Complete = true;
            if (requestModel.VerifyResult == VerifyResultAccept)
            {
                var player = matchup.Players.First(x => x.PublicId == requestModel.PlayerPublicId);
                mapSelection.WinnerName = player.Name;
                mapSelection.WinnerFinishedAt = player.CompleteTime;
                mapSelection.ResultVerifiedAt = DateTime.UtcNow;
            }
            else if (matchup.Players.All(x => x.CompleteTime.HasValue))
            {
                // In a draw, grab the faster player's time (assuming photo finish draw; timeouts won't have any valid finishers)
                var fasterPlayer = matchup.Players.OrderBy(x => x.CompleteTime).First();
                mapSelection.WinnerFinishedAt = fasterPlayer.CompleteTime;
            }
            
            matchup.Players.ForEach(x => x.CompleteTime = null);
        }
        dbContext.Update(matchup);
        await dbContext.SaveChangesAsync();

        if (requestModel.VerifyResult == VerifyResultAccept || requestModel.VerifyResult == VerifyResultDraw)
        {
            matchup = await ClearSpinAsync(matchupId, userContext);
        }

        return matchup;
    }

    public async Task<Matchup> DeleteMostRecentResult(Guid matchupId, int mapSelectionId, DiscordUserContext? userContext)
    {
        var matchup = GetMatchup(matchupId)!;

        if (matchup.HitmapsTournamentsId.HasValue)
        {
            await hitmapsTournamentsService.IsUserAuthorizedForMatchAsync(matchup.HitmapsTournamentsId.Value, userContext);
        }

        var mostRecentlyCompletedMap = matchup.MapSelections
            .Where(x => x.Complete)
            .OrderByDescending(x => x.Id)
            .First();

        if (mostRecentlyCompletedMap.Id != mapSelectionId)
        {
            throw new ConflictException<int>(mostRecentlyCompletedMap.Id, mapSelectionId);
        }

        mostRecentlyCompletedMap.Complete = false;
        mostRecentlyCompletedMap.WinnerName = null;
        dbContext.Update(matchup);
        await dbContext.SaveChangesAsync();

        return matchup;
    }

    public async Task<Matchup> UpdateForfeitAsync(Guid matchupId, string forfeitPlayerId, ForfeitRequestModel requestModel, DiscordUserContext? userContext)
    {
        var matchup = GetMatchup(matchupId)!;
        
        if (matchup.HitmapsTournamentsId.HasValue)
        {
            await hitmapsTournamentsService.IsUserAuthorizedForMatchAsync(matchup.HitmapsTournamentsId.Value, userContext);
        }

        matchup.Players.First(x => x.PublicId == forfeitPlayerId).Forfeit = requestModel.Forfeit;

        dbContext.Update(matchup);
        await dbContext.SaveChangesAsync();

        return matchup;
    }

    public async Task UpdateObjectivesAsync(Guid matchupId, UpdateObjectivesRequestModelCollection requestModel, DiscordUserContext? userContext)
    {
        var matchup = GetBasicMatchup(matchupId)!;
        
        if (matchup.HitmapsTournamentsId.HasValue)
        {
            await hitmapsTournamentsService.IsUserAuthorizedForMatchAsync(matchup.HitmapsTournamentsId.Value, userContext);
        }

        await completedObjectiveRepository.UpdateObjectivesAsync(requestModel.ToDbCompletedObjectives(matchup.Id));
    }

    public async Task<Matchup> ClearObjectivesAsync(Guid matchupId, DiscordUserContext? userContext)
    {
        var matchup = GetMatchup(matchupId)!;
        
        if (matchup.HitmapsTournamentsId.HasValue)
        {
            await hitmapsTournamentsService.IsUserAuthorizedForMatchAsync(matchup.HitmapsTournamentsId.Value, userContext);
        }
        
        matchup.CompletedObjectives.Clear();

        dbContext.Update(matchup);
        await dbContext.SaveChangesAsync();

        return matchup;
    }

    public List<Matchup> GetCompletedMatchupsForIds(List<Guid> matchIds)
    {
        //@formatter:off
        return dbContext.Matchups
            .Include(x => x.Players)
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Mission)
            .Include("MapSelections.SavedSpin")
            .Include("MapSelections.SavedSpin.Mission")
            .Include("MapSelections.SavedSpin.Mission.Targets")
            .Include("MapSelections.SavedSpin.Mission.AdditionalObjectives")
            .Include("MapSelections.SavedSpin.Mission.AdditionalObjectives.CompletionOptions")
            .Include("MapSelections.SavedSpin.SavedConditions")
            .Where(x => matchIds.Contains(x.MatchupId))
            .ToList();
        //@formatter:on
    }
}