using DataAccess.Models.EF;
using RouletteBackend.BusinessLogic.KillMethods;
using RouletteBackend.Controllers;
using RouletteBackend.Controllers.ViewModels;
using RouletteBackend.ViewModels;

namespace RouletteBackend.BusinessLogic;

public interface ISpinService
{
    public Task<List<KillCategory>> FetchKillMethodsAsync(FetchKillMethodsRequestModel requestModel);
    public List<AdditionalObjectiveOption> FetchAdditionalObjectiveCompletionMethods(
        FetchAdditionalObjectivesRequestModel requestModel);
    public Task<GeneratedSpin> GenerateSpinAsync(GenerateSpinRequestModel requestModel);
}