namespace RouletteBackend.BusinessLogic;

public class MissingMissionException : Exception
{
    public MissingMissionException(string missionSlug) : base($"Mission {missionSlug} not found.")
    {
    }
}