using DataAccess.Data;
using DataAccess.Models.EF.Matchups;
using Microsoft.EntityFrameworkCore;
using RouletteBackend.BusinessLogic.Discord;
using RouletteBackend.Controllers.ViewModels.Matchups;

namespace RouletteBackend.BusinessLogic;

public class MessageService : IMessageService
{
    private readonly RouletteDbContext dbContext;
    private readonly IHitmapsTournamentsService hitmapsTournamentsService;
    private const string ActionReceive = "Receive";
    private const string ActionAcknowledge = "Acknowledge";


    public MessageService(RouletteDbContext dbContext, IHitmapsTournamentsService hitmapsTournamentsService)
    {
        this.dbContext = dbContext;
        this.hitmapsTournamentsService = hitmapsTournamentsService;
    }

    public Message? GetUnacknowledgedMessage(Guid matchupId, string playerPublicId)
    {
        var matchup = dbContext.Matchups
            .Include(x => x.Players)
            .ThenInclude(x => x.Messages)
            .Include(x => x.CurrentSpin)
            .First(x => x.MatchupId == matchupId);

        return matchup.Players
            .First(x => x.PublicId == playerPublicId).Messages
            .FirstOrDefault(x => x.AcknowledgedAt == null);
    }

    private async Task<Matchup> GetSecuredMatchupAsync(Guid matchupId, DiscordUserContext? userContext)
    {
        var matchup = dbContext.Matchups
            .Include(x => x.Players)
            .ThenInclude(x => x.Messages)
            .Include(x => x.CurrentSpin)
            .First(x => x.MatchupId == matchupId);

        if (matchup.HitmapsTournamentsId.HasValue)
        {
            await hitmapsTournamentsService.IsUserAuthorizedForMatchAsync(matchup.HitmapsTournamentsId.Value, userContext);
        }

        return matchup;
    }

    public async Task<Message> SendMessageAsync(Guid matchupId, SendMessageRequestModel requestModel, DiscordUserContext? userContext)
    {
        var matchup = await GetSecuredMatchupAsync(matchupId, userContext);
        var player = matchup.Players.First(x => x.PublicId == requestModel.PlayerPublicId);

        if (player.Messages.Any(x => !x.AcknowledgedAt.HasValue))
        {
            throw new InvalidOperationException("Cannot send a new message when one is still not acknowledged!");
        }

        var message = new Message
        {
            Player = player,
            Content = requestModel.Message,
            SentAt = DateTime.UtcNow
        };

        dbContext.Update(message);
        await dbContext.SaveChangesAsync();

        return message;
    }

    public Message ParticipantHandleMessage(Guid matchupId, string playerPublicId, ParticipantHandleMessageRequestModel requestModel)
    {
        var matchup = dbContext.Matchups
            .Include(x => x.Players)
            .ThenInclude(x => x.Messages)
            .Include(x => x.CurrentSpin)
            .First(x => x.MatchupId == matchupId);

        var message = matchup.Players.First(x => x.PublicId == playerPublicId).Messages
            .First(x => x.Id == requestModel.Id);

        if (requestModel.Action == ActionReceive && !message.ReceivedAt.HasValue)
        {
            message.ReceivedAt = DateTime.UtcNow;
        }

        if (requestModel.Action == ActionAcknowledge && !message.AcknowledgedAt.HasValue)
        {
            message.AcknowledgedAt = DateTime.UtcNow;
        }

        dbContext.Update(message);
        dbContext.SaveChanges();

        return message;
    }
}