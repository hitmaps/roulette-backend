namespace RouletteBackend.BusinessLogic.Discord;

public interface IDiscordService
{
    Task<string?> VerifyTokenAsync(string tokenType, string token);
}