using System.Net;
using System.Net.Http.Headers;
using System.Text.Json;

namespace RouletteBackend.BusinessLogic.Discord;

public class DiscordService : IDiscordService
{
    private readonly HttpClientWrapper httpClientWrapper;

    public DiscordService(HttpClientWrapper httpClientWrapper)
    {
        this.httpClientWrapper = httpClientWrapper;
    }

    public async Task<string?> VerifyTokenAsync(string tokenType, string token)
    {
        using var request = new HttpRequestMessage(HttpMethod.Get,
            "https://discordapp.com/api/v9/users/@me");
        request.Headers.Authorization = new AuthenticationHeaderValue(tokenType, token);
        var response = await httpClientWrapper.GetClient().SendAsync(request);

        if (response.StatusCode != HttpStatusCode.OK)
        {
            return null;
        }

        return JsonSerializer.Deserialize<User>(await response.Content.ReadAsStreamAsync(),
            new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            })?.Id;
    }
}