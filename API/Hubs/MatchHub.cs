using Microsoft.AspNetCore.SignalR;
using RouletteBackend.BusinessLogic;
using RouletteBackend.Controllers.ViewModels;
using RouletteBackend.Hubs.ViewModels;

namespace RouletteBackend.Hubs;

public class MatchHub : Hub<IMatchHub>
{
    public const string MatchParticipantsGroupFormat = "match-participant-{0}";
    public const string MatchAdminGroupFormat = "match-admin-{0}";
    public const string MatchParticipantMessageGroupFormat = "match-participant-message-{0}-{1}";
    public const string MatchAdminMessageAckGroupFormat = "match-admin-message-ack-{0}";
    private readonly IMatchupService matchupService;

    public MatchHub(IMatchupService matchupService)
    {
        this.matchupService = matchupService;
    }

    public async Task JoinMatchGroup(Guid matchupId)
    {
        await Groups.AddToGroupAsync(Context.ConnectionId, string.Format(MatchParticipantsGroupFormat, matchupId.ToString()));
    }

    public async Task AdminJoinMatchGroup(Guid matchupId)
    {
        await Groups.AddToGroupAsync(Context.ConnectionId, string.Format(MatchAdminGroupFormat, matchupId.ToString()));
    }

    public async Task LeaveMatchGroup(Guid matchupId)
    {
        await Groups.RemoveFromGroupAsync(Context.ConnectionId, string.Format(MatchParticipantsGroupFormat, matchupId.ToString()));
    }

    public async Task AdminLeaveMatchGroup(Guid matchupId)
    {
        await Groups.RemoveFromGroupAsync(Context.ConnectionId, string.Format(MatchAdminGroupFormat, matchupId.ToString()));
    }

    public async Task AdminJoinMessageAck(Guid matchupId)
    {
        await Groups.AddToGroupAsync(Context.ConnectionId,
            string.Format(MatchAdminMessageAckGroupFormat, matchupId.ToString()));
    }

    public async Task AdminLeaveMessageAck(Guid matchupId)
    {
        await Groups.RemoveFromGroupAsync(Context.ConnectionId,
            string.Format(MatchAdminMessageAckGroupFormat, matchupId.ToString()));
    }

    public async Task ParticipantJoinMessageGroup(Guid matchupId, string playerId)
    {
        await Groups.AddToGroupAsync(Context.ConnectionId,
            string.Format(MatchParticipantMessageGroupFormat, matchupId.ToString(), playerId));
    }

    public async Task ParticipantLeaveMessageGroup(Guid matchupId, string playerId)
    {
        await Groups.RemoveFromGroupAsync(Context.ConnectionId,
            string.Format(MatchParticipantMessageGroupFormat, matchupId.ToString(), playerId));
    }

    public HeartbeatStateViewModel SendHeartbeat(Guid matchupId, string playerId, Guid timerFingerprint)
    {
        var matchup = matchupService.GetMatchup(matchupId, playerId);

        if (matchup == null)
        {
            return new HeartbeatStateViewModel
            {
                State = "NOTFOUND"
            };
        }
        
        var player = matchup.Players.First(x => x.PublicId == playerId);
        Clients.Group(string.Format(MatchAdminGroupFormat, matchupId))
            .HeartbeatUpdate(ParticipantViewModel.FromDbModel(player));

        if (matchup.TimerFingerprint != timerFingerprint)
        {
            return new HeartbeatStateViewModel
            {
                State = "DESYNC",
                Match = MatchupViewModel.FromDbModel(matchup)
            };
        }

        return new HeartbeatStateViewModel
        {
            State = "SYNC"
        };
    }
}