using RouletteBackend.Controllers.ViewModels;
using RouletteBackend.Controllers.ViewModels.Matchups;

namespace RouletteBackend.Hubs;

public interface IMatchHub
{
    public Task MatchChange(MatchupViewModel matchup);

    public Task SpinComplete(ParticipantViewModel player);

    public Task MessageAcknowledged(MessageViewModel message);

    public Task NewMessage(MessageViewModel message);

    public Task HeartbeatUpdate(ParticipantViewModel player);

    public Task ObjectiveUpdate(UpdateObjectivesRequestModelCollection viewModel);
}