using RouletteBackend.Controllers.ViewModels;

namespace RouletteBackend.Hubs.ViewModels;

public class HeartbeatStateViewModel
{
    public string State { get; set; } = null!;
    public MatchupViewModel? Match { get; set; }
}